package fr.m2ihm.lnso.tripjournal.services;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.IBinder;
import android.widget.Toast;

import com.google.android.gms.maps.model.LatLng;

import fr.m2ihm.lnso.tripjournal.R;
import fr.m2ihm.lnso.tripjournal.activities.TripActivity;
import fr.m2ihm.lnso.tripjournal.database.DatabaseHelper;
import fr.m2ihm.lnso.tripjournal.database.LocationDAO;
import fr.m2ihm.lnso.tripjournal.database.TripDAO;
import fr.m2ihm.lnso.tripjournal.entities.LocationEntity;

public class LocationTrackerService extends Service {

	public static final String EXTRA_CONNECTION_STATE = "connection_state";
	public static final String ACTION_CONNECTION_STATE = "connection_state_display";
	
	private LocationManager mLocationManager;
	private LocationListener mLocationListener;

	private DatabaseHelper mDatabaseHelper;
	private static long mTripId = TripActivity.NEW_TRIP_ID;
	
	
	@Override
	public void onCreate() {		
		// Acquire a reference to the system Location Manager
		mLocationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

		// Define a listener that is triggered when location changes
		mLocationListener = new LocationListener() {
			@Override
			public void onLocationChanged(Location location) {
				
				if (checkConnection(getBaseContext())) {					
					// When the location has changed, check if the trip id is correct
					if (mTripId != TripActivity.NEW_TRIP_ID) {
						mDatabaseHelper = DatabaseHelper.getInstance(getApplicationContext());
						TripDAO tripDAO = new TripDAO(mDatabaseHelper.getReadableDatabase());
						// Check is the trip still exist (has not been deleted by the user)
						if (tripDAO.select(mTripId).getCount() != 0) {
							LocationDAO locationDAO = new LocationDAO(mDatabaseHelper.getWritableDatabase());
							LocationEntity locationEntity = new LocationEntity(
									TripActivity.NEW_TRIP_ID,
									new LatLng(location.getLatitude(), location.getLongitude()),
									mTripId);
							locationDAO.insert(locationEntity);
							
							// Send a message to the activity to display the connection state
							Intent intent = new Intent(ACTION_CONNECTION_STATE);
							intent.putExtra(EXTRA_CONNECTION_STATE, "");
							getApplicationContext().sendBroadcast(intent);
							
						}
						else {
							// If the trip has been deleted by the user there is no point of recording location anymore
							LocationTrackerService.this.stopSelf();
						}
					}
				} else {
					// Send a message to the activity to display the connection state
					Intent intent = new Intent(ACTION_CONNECTION_STATE);
					intent.putExtra(EXTRA_CONNECTION_STATE, getResources().getString(R.string.alert_no_connection));
					getApplicationContext().sendBroadcast(intent);
				}
			}
			
			@Override
			public void onProviderEnabled(String provider) {}
			
			@Override
			public void onProviderDisabled(String provider) {}
			
			@Override
			public void onStatusChanged(String provider, int status, Bundle extras) {}
		};

	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		Toast.makeText(this, getResources().getString(R.string.service_started), Toast.LENGTH_SHORT).show();
		mTripId = intent.getLongExtra(TripActivity.EXTRA_TRIP_ID, TripActivity.NEW_TRIP_ID);
		mLocationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 10, mLocationListener);
		return START_STICKY;
	}
	
	@Override
	public IBinder onBind(Intent intent) {
		// We don't provide binding, so return null
		return null;
	}
	
	@Override
	public void onDestroy() {
		Toast.makeText(this, getResources().getString(R.string.service_stoped), Toast.LENGTH_SHORT).show(); 
		mLocationManager.removeUpdates(mLocationListener);
	}

    /**
    * Simple network connection check.
    *
    * @param context
    * @return False if no connection, true otherwise
    */
    private boolean checkConnection(Context context) {
        final ConnectivityManager cm =
                (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        final NetworkInfo networkInfo = cm.getActiveNetworkInfo();
        if (networkInfo == null || !networkInfo.isConnectedOrConnecting()) {
            return false;
        }
        return true;
    }

	public static long getmTripId() {
		return mTripId;
	}

	public static void setmTripId(long mTripId) {
		LocationTrackerService.mTripId = mTripId;
	}
}