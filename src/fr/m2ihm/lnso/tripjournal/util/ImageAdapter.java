package fr.m2ihm.lnso.tripjournal.util;

import java.io.FileNotFoundException;

import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import fr.m2ihm.lnso.tripjournal.R;

public class ImageAdapter extends BaseAdapter {
	private Context mContext;
	private Uri [] mImageUris;

	public ImageAdapter(Context c, Uri [] imageUris) {
		mContext = c;
		mImageUris = imageUris;
	}

	public int getCount() {
		return mImageUris.length;
	}

	public Object getItem(int position) {
		return null;
	}

	public long getItemId(int position) {
		return 0;
	}

	// create a new ImageView for each item referenced by the Adapter
	public View getView(int position, View convertView, ViewGroup parent) {
		ImageView imageView;
		if (convertView == null) { // if it's not recycled, initialize some
									// attributes
			imageView = new ImageView(mContext);
			imageView.setLayoutParams(new GridView.LayoutParams(85, 85));
			imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
			// imageView.setPadding(8, 8, 8, 8);
		} else {
			imageView = (ImageView) convertView;
		}

		Bitmap imageBitmap = ImageResizer.decodeSampledBitmapFromResource(
				mContext.getResources(),
				R.drawable.default_picture,
				imageView.getLayoutParams().width,
				imageView.getLayoutParams().height);
		try {
			imageBitmap = ImageResizer.decodeSampledBitmapFromUri(
					mContext,
					mImageUris[position],
					imageView.getLayoutParams().width,
					imageView.getLayoutParams().height);
			imageView.setImageBitmap(imageBitmap);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} finally {
			imageView.setImageBitmap(imageBitmap);
		}

		return imageView;
	}

}
