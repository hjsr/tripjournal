package fr.m2ihm.lnso.tripjournal.database;

import fr.m2ihm.lnso.tripjournal.entities.Participant;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public class ParticipantDAO {
	
	private SQLiteDatabase mDatabase;	
	
	private static final String PARTICIPANT_SELECT_ALL_BY_EVENT_ID = "SELECT * FROM "
			+ DatabaseHelper.PARTICIPANT_TABLE_NAME + " WHERE "
			+ DatabaseHelper.PARTICIPANT_EVENT_ID + " = ?;";
	
	private static final String PARTICIPANT_DELETE_ALL_BY_EVENT_ID =
			DatabaseHelper.PARTICIPANT_EVENT_ID + " = ?;";
	
	public ParticipantDAO(SQLiteDatabase mDatabase) {
		this.mDatabase = mDatabase;
	}

	public long insert(Participant participant) {
		ContentValues values = new ContentValues();
		values.put(DatabaseHelper.PARTICIPANT_CONTACT_ID, participant.getmIdContact());
		values.put(DatabaseHelper.PARTICIPANT_CONTACT_NAME, participant.getmName());
		values.put(DatabaseHelper.PARTICIPANT_EVENT_ID, participant.getmIdEvent());
		return mDatabase.insert(DatabaseHelper.PARTICIPANT_TABLE_NAME, null, values);
	}
	
	public int delete(long id) {
		return mDatabase.delete(DatabaseHelper.PARTICIPANT_TABLE_NAME, DatabaseHelper.PARTICIPANT_KEY + " = ?", new String[] {String.valueOf(id)});
	}
	
	public long update(Participant participant) {
		ContentValues values = new ContentValues();
		values.put(DatabaseHelper.PARTICIPANT_CONTACT_ID, participant.getmIdContact());
		values.put(DatabaseHelper.PARTICIPANT_CONTACT_NAME, participant.getmName());
		values.put(DatabaseHelper.PARTICIPANT_EVENT_ID, participant.getmIdEvent());
		return mDatabase.update(DatabaseHelper.PARTICIPANT_TABLE_NAME, values, DatabaseHelper.PARTICIPANT_KEY + " = ?", new String[] {String.valueOf(participant.getmId())});
	}
	
	public Cursor selectAllParticipantForAnEventId(long idEvent) {
		return mDatabase.rawQuery(ParticipantDAO.PARTICIPANT_SELECT_ALL_BY_EVENT_ID, new String[] {String.valueOf(idEvent)});
	}
	
	public Cursor selectAll() {
		return mDatabase.rawQuery("select * from " + DatabaseHelper.PARTICIPANT_TABLE_NAME, null);
	}
	
	public int deleteAllParticipantForAnEventId(long idEvent) {
		return mDatabase.delete(DatabaseHelper.PARTICIPANT_TABLE_NAME, ParticipantDAO.PARTICIPANT_DELETE_ALL_BY_EVENT_ID, new String[] {String.valueOf(idEvent)});
	}
	
}
