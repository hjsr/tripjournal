package fr.m2ihm.lnso.tripjournal.database;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import fr.m2ihm.lnso.tripjournal.entities.LocationEntity;

public class LocationDAO {
	
	private SQLiteDatabase mDatabase;
	
	private static final String LOCATION_SELECT = "SELECT * FROM "
			+ DatabaseHelper.LOCATION_TABLE_NAME + " WHERE "
			+ DatabaseHelper.LOCATION_KEY + " = ?;";
	
	private static final String LOCATION_SELECT_ALL_BY_TRIP_ID = "SELECT * FROM "
			+ DatabaseHelper.LOCATION_TABLE_NAME + " WHERE "
			+ DatabaseHelper.LOCATION_TRIP_ID + " = ?;";
	
	private static final String LOCATION_DELETE_BY_ID =
			DatabaseHelper.LOCATION_KEY + " = ?;";
	
	public LocationDAO(SQLiteDatabase mDatabase) {
		this.mDatabase = mDatabase;
	}

	public Cursor select(long idLocation) {
		return mDatabase.rawQuery(LocationDAO.LOCATION_SELECT, new String[] {String.valueOf(idLocation)});
	}
	
	public long insert(LocationEntity location) {		
		ContentValues values = new ContentValues();
		values.put(DatabaseHelper.LOCATION_TRIP_ID, location.getmIdTrip());
		values.put(DatabaseHelper.LOCATION_LONGITUDE, location.getmLocation().longitude);
		values.put(DatabaseHelper.LOCATION_LATITUDE, location.getmLocation().latitude);
		return mDatabase.insert(DatabaseHelper.LOCATION_TABLE_NAME, null, values);
	}
	
	public int delete(long idLocation) {
		return mDatabase.delete(DatabaseHelper.LOCATION_TABLE_NAME, LocationDAO.LOCATION_DELETE_BY_ID, new String[] {String.valueOf(idLocation)});
	}
	
	public long update(LocationEntity location) {
		ContentValues values = new ContentValues();
		values.put(DatabaseHelper.LOCATION_TRIP_ID, location.getmIdTrip());
		values.put(DatabaseHelper.LOCATION_LONGITUDE, location.getmLocation().longitude);
		values.put(DatabaseHelper.LOCATION_LATITUDE, location.getmLocation().latitude);
		return mDatabase.update(DatabaseHelper.LOCATION_TABLE_NAME, values, DatabaseHelper.LOCATION_KEY + " = ?", new String[] {String.valueOf(location.getmId())});
	}
	
	public Cursor selectAllLocationsForATripId(long idTrip) {
		return mDatabase.rawQuery(LocationDAO.LOCATION_SELECT_ALL_BY_TRIP_ID, new String[] {String.valueOf(idTrip)});
	}
}
