package fr.m2ihm.lnso.tripjournal.database;

import fr.m2ihm.lnso.tripjournal.entities.Trip;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public class TripDAO {
	
	private SQLiteDatabase mDatabase;
	
	private static final String TRIP_SELECT = "SELECT * FROM "
			+ DatabaseHelper.TRIP_TABLE_NAME + " WHERE "
			+ DatabaseHelper.TRIP_KEY + " = ?;";
	
	private static final String TRIP_SELECT_ALL = "SELECT * FROM "
			+ DatabaseHelper.TRIP_TABLE_NAME + " ORDER BY " 
			+ DatabaseHelper.TRIP_START_DATE + " DESC;";
	
	private static final String TRIP_DELETE_BY_ID =
			DatabaseHelper.TRIP_KEY + " = ?;";
	
	private static final String TRIP_DELETE_EVENTS = 
			DatabaseHelper.EVENT_TABLE_NAME + "." + DatabaseHelper.EVENT_TRIP_ID + " = ?;";
	
	private static final String TRIP_DELETE_PARTICIPANTS = 
			DatabaseHelper.PARTICIPANT_TABLE_NAME + "." + DatabaseHelper.PARTICIPANT_EVENT_ID + " in (SELECT "
			+ DatabaseHelper.EVENT_TABLE_NAME + "." + DatabaseHelper.EVENT_KEY + " FROM "
			+ DatabaseHelper.EVENT_TABLE_NAME + " WHERE "
			+ DatabaseHelper.EVENT_TABLE_NAME + "." + DatabaseHelper.EVENT_TRIP_ID + " = ?);";
	
	private static final String TRIP_DELETE_LOCATIONS = 
			DatabaseHelper.LOCATION_TABLE_NAME + "." + DatabaseHelper.LOCATION_TRIP_ID + " = ?;";
	
	public TripDAO(SQLiteDatabase mDatabase) {
		this.mDatabase = mDatabase;
	}

	public Cursor select(long idTrip) {
		return mDatabase.rawQuery(TripDAO.TRIP_SELECT, new String[] {String.valueOf(idTrip)});
	}
	
	public long insert(Trip trip) {
		ContentValues values = new ContentValues();
		values.put(DatabaseHelper.TRIP_NAME, trip.getmName());
		values.put(DatabaseHelper.TRIP_START_DATE, trip.getmStartDate());
		values.put(DatabaseHelper.TRIP_END_DATE, trip.getmEndDate());
		return mDatabase.insert(DatabaseHelper.TRIP_TABLE_NAME, null, values);
	}
	
	public int delete(long idTrip) {
		return mDatabase.delete(DatabaseHelper.PARTICIPANT_TABLE_NAME, TripDAO.TRIP_DELETE_PARTICIPANTS, new String[] {String.valueOf(idTrip)})
			+ mDatabase.delete(DatabaseHelper.EVENT_TABLE_NAME, TripDAO.TRIP_DELETE_EVENTS, new String[] {String.valueOf(idTrip)})
			+ mDatabase.delete(DatabaseHelper.LOCATION_TABLE_NAME, TripDAO.TRIP_DELETE_LOCATIONS, new String[] {String.valueOf(idTrip)})
			+ mDatabase.delete(DatabaseHelper.TRIP_TABLE_NAME, TripDAO.TRIP_DELETE_BY_ID, new String[] {String.valueOf(idTrip)});
	}
	
	public long update(Trip trip) {
		ContentValues values = new ContentValues();
		values.put(DatabaseHelper.TRIP_NAME, trip.getmName());
		values.put(DatabaseHelper.TRIP_START_DATE, Long.toString(trip.getmStartDate()));
		values.put(DatabaseHelper.TRIP_END_DATE, Long.toString(trip.getmEndDate()));
		return mDatabase.update(DatabaseHelper.TRIP_TABLE_NAME, values, DatabaseHelper.TRIP_KEY + " = ?", new String[] {String.valueOf(trip.getmId())});
	}
	
	public Cursor selectAllTrips() {
		return mDatabase.rawQuery(TripDAO.TRIP_SELECT_ALL, null);
	}
	
}
