package fr.m2ihm.lnso.tripjournal.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DatabaseHelper extends SQLiteOpenHelper {
	
	private static DatabaseHelper mInstance = null;
	
	public final static int VERSION = 6;
	public final static String NAME = "database.db";
	
	//-------------------------------------------------------

	public static final String TRIP_KEY = "_id";
	public static final String TRIP_NAME = "name";
	public static final String TRIP_START_DATE = "start_date";
	public static final String TRIP_END_DATE = "end_date";
	
	public static final String TRIP_TABLE_NAME = "Trip";
	public static final String TRIP_TABLE_CREATE = "CREATE TABLE " + TRIP_TABLE_NAME + " ("
			+ TRIP_KEY + " INTEGER PRIMARY KEY AUTOINCREMENT, "
			+ TRIP_NAME + " TEXT NOT NULL, "
			+ TRIP_START_DATE + " INTEGER NOT NULL, "
			+ TRIP_END_DATE + " INTEGER NOT NULL);";
	public static final String TRIP_TABLE_DROP = "DROP TABLE IF EXISTS " + TRIP_TABLE_NAME + ";";
	
	//-------------------------------------------------------
	
	public static final String EVENT_KEY = "_id";
	public static final String EVENT_TRIP_ID = "id_trip";
	public static final String EVENT_NAME = "name";
	public static final String EVENT_DESCRIPTION = "description";
	public static final String EVENT_DATE = "date";
	public static final String EVENT_TYPE = "type";
	public static final String EVENT_MARK = "mark";
	public static final String EVENT_PICTURE = "picture";
	public static final String EVENT_LATITUDE = "latitude";
	public static final String EVENT_LONGITUDE = "longitude";
	
	public static final String EVENT_TABLE_NAME = "Event";
	public static final String EVENT_TABLE_CREATE = "CREATE TABLE " + EVENT_TABLE_NAME + " ("
			+ EVENT_KEY + " INTEGER PRIMARY KEY AUTOINCREMENT, "
			+ EVENT_TRIP_ID + " INTEGER NOT NULL, "
			+ EVENT_NAME + " TEXT NOT NULL, "
			+ EVENT_DESCRIPTION + " TEXT NOT NULL, "
			+ EVENT_DATE + " INTEGER NOT NULL, "
			+ EVENT_TYPE + " TEXT NOT NULL, "
			+ EVENT_MARK + " REAL NOT NULL, "
			+ EVENT_PICTURE + " TEXT, "
			+ EVENT_LATITUDE + " REAL, "
			+ EVENT_LONGITUDE + " REAL);";
	public static final String EVENT_TABLE_DROP = "DROP TABLE IF EXISTS " + EVENT_TABLE_NAME + ";";
	
	//-------------------------------------------------------
	
	public static final String PARTICIPANT_KEY = "id";
	public static final String PARTICIPANT_CONTACT_ID = "id_contact";
	public static final String PARTICIPANT_CONTACT_NAME = "name_contact";
	public static final String PARTICIPANT_EVENT_ID = "event_id";
	
	public static final String PARTICIPANT_TABLE_NAME = "Participant";
	public static final String PARTICIPANT_TABLE_CREATE = "CREATE TABLE " + PARTICIPANT_TABLE_NAME + " ("
			+ PARTICIPANT_KEY + " INTEGER PRIMARY KEY AUTOINCREMENT, "
			+ PARTICIPANT_CONTACT_ID + " TEXT NOT NULL, "
			+ PARTICIPANT_CONTACT_NAME + " TEXT NOT NULL, "
			+ PARTICIPANT_EVENT_ID + " INTEGER NOT NULL);";
	public static final String PARTICIPANT_TABLE_DROP = "DROP TABLE IF EXISTS " + PARTICIPANT_TABLE_NAME + ";";
	
	//-------------------------------------------------------
	
	public static final String LOCATION_KEY = "id";
	public static final String LOCATION_TRIP_ID = "id_trip";
	public static final String LOCATION_LONGITUDE = "longitude";
	public static final String LOCATION_LATITUDE = "latitude";
	
	public static final String LOCATION_TABLE_NAME = "Location";
	public static final String LOCATION_TABLE_CREATE = "CREATE TABLE " + LOCATION_TABLE_NAME + " ("
			+ LOCATION_KEY + " INTEGER PRIMARY KEY AUTOINCREMENT, "
			+ LOCATION_TRIP_ID + " INTEGER NOT NULL, "
			+ LOCATION_LONGITUDE + " REAL NOT NULL, "
			+ LOCATION_LATITUDE + " REAL NOT NULL);";
	public static final String LOCATION_TABLE_DROP = "DROP TABLE IF EXISTS " + LOCATION_TABLE_NAME + ";";
	
	//-------------------------------------------------------

	public static DatabaseHelper getInstance(Context context) {
		if (mInstance == null) {
			mInstance = new DatabaseHelper(context.getApplicationContext());
		}
		return mInstance;
	}
	
	private DatabaseHelper(Context context) {
		super(context, DatabaseHelper.NAME, null, DatabaseHelper.VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL(TRIP_TABLE_CREATE);
		db.execSQL(EVENT_TABLE_CREATE);
		db.execSQL(PARTICIPANT_TABLE_CREATE);
		db.execSQL(LOCATION_TABLE_CREATE);
	}
	
	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		db.execSQL(TRIP_TABLE_DROP);
		db.execSQL(EVENT_TABLE_DROP);
		db.execSQL(PARTICIPANT_TABLE_DROP);
		db.execSQL(LOCATION_TABLE_DROP);
		onCreate(db);
	}

}
