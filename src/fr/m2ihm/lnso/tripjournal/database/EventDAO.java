package fr.m2ihm.lnso.tripjournal.database;

import fr.m2ihm.lnso.tripjournal.entities.Event;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public class EventDAO {
	
	private SQLiteDatabase mDatabase;
	
	private static final String EVENT_SELECT = "SELECT * FROM "
			+ DatabaseHelper.EVENT_TABLE_NAME + " WHERE "
			+ DatabaseHelper.EVENT_KEY + " = ?;";
	
	private static final String EVENT_SELECT_ALL_BY_TRIP_ID = "SELECT * FROM "
			+ DatabaseHelper.EVENT_TABLE_NAME + " WHERE "
			+ DatabaseHelper.EVENT_TRIP_ID + " = ? ORDER BY " 
			+ DatabaseHelper.EVENT_DATE + " DESC;";
	
	private static final String EVENT_SELECT_ALL_PLACES_WITH_TRIP_ID = "SELECT "
			+ DatabaseHelper.EVENT_TRIP_ID + ", " 
			+ DatabaseHelper.EVENT_LATITUDE + ", "
			+ DatabaseHelper.EVENT_LONGITUDE + " FROM "
			+ DatabaseHelper.EVENT_TABLE_NAME + ";";
	
	private static final String EVENT_SELECT_ALL_PICTURES_WITH_TRIP_ID = "SELECT "
			+ DatabaseHelper.EVENT_PICTURE + " FROM "
			+ DatabaseHelper.EVENT_TABLE_NAME + " WHERE "
			+ DatabaseHelper.EVENT_TRIP_ID + " = ?;";
	
	private static final String EVENT_DELETE_BY_ID =
			DatabaseHelper.EVENT_KEY + " = ?;";
	
	private static final String EVENT_DELETE_PARTICIPANTS =
			DatabaseHelper.PARTICIPANT_EVENT_ID + " = ?;";
	
	public EventDAO(SQLiteDatabase mDatabase) {
		this.mDatabase = mDatabase;
	}

	public Cursor select(long idEvent) {
		return mDatabase.rawQuery(EventDAO.EVENT_SELECT, new String[] {String.valueOf(idEvent)});
	}
	
	public long insert(Event event) {		
		ContentValues values = new ContentValues();
		values.put(DatabaseHelper.EVENT_TRIP_ID, event.getmIdTrip());
		values.put(DatabaseHelper.EVENT_NAME, event.getmName());
		values.put(DatabaseHelper.EVENT_DESCRIPTION, event.getmDescription());
		values.put(DatabaseHelper.EVENT_DATE, event.getmDate());
		values.put(DatabaseHelper.EVENT_TYPE, event.getmType().name());
		values.put(DatabaseHelper.EVENT_MARK, event.getmMark());
		values.put(DatabaseHelper.EVENT_PICTURE, event.getmPicture());
		values.put(DatabaseHelper.EVENT_LATITUDE, event.getmLatitude());
		values.put(DatabaseHelper.EVENT_LONGITUDE, event.getmLongitude());
		return mDatabase.insert(DatabaseHelper.EVENT_TABLE_NAME, null, values);
	}
	
	public int delete(long idEvent) {
		return mDatabase.delete(DatabaseHelper.PARTICIPANT_TABLE_NAME, EventDAO.EVENT_DELETE_PARTICIPANTS, new String[] {String.valueOf(idEvent)})
			+ mDatabase.delete(DatabaseHelper.EVENT_TABLE_NAME, EventDAO.EVENT_DELETE_BY_ID, new String[] {String.valueOf(idEvent)});
	}
	
	public long update(Event event) {
		ContentValues values = new ContentValues();
		values.put(DatabaseHelper.EVENT_TRIP_ID, event.getmIdTrip());
		values.put(DatabaseHelper.EVENT_NAME, event.getmName());
		values.put(DatabaseHelper.EVENT_DESCRIPTION, event.getmDescription());
		values.put(DatabaseHelper.EVENT_DATE, event.getmDate());
		values.put(DatabaseHelper.EVENT_TYPE, event.getmType().name());
		values.put(DatabaseHelper.EVENT_MARK, event.getmMark());
		values.put(DatabaseHelper.EVENT_PICTURE, event.getmPicture());
		values.put(DatabaseHelper.EVENT_LATITUDE, event.getmLatitude());
		values.put(DatabaseHelper.EVENT_LONGITUDE, event.getmLongitude());
		return mDatabase.update(DatabaseHelper.EVENT_TABLE_NAME, values, DatabaseHelper.EVENT_KEY + " = ?", new String[] {String.valueOf(event.getmId())});
	}
	
	public Cursor selectAllEventsForATripId(long idTrip) {
		return mDatabase.rawQuery(EventDAO.EVENT_SELECT_ALL_BY_TRIP_ID, new String[] {String.valueOf(idTrip)});
	}
	
	public Cursor selectAllEventsPlacesWithTripId() {
		return mDatabase.rawQuery(EventDAO.EVENT_SELECT_ALL_PLACES_WITH_TRIP_ID, null);
	}
	
	public Cursor selectAllEventsPicturesWithTripId(long idTrip) {
		return mDatabase.rawQuery(EventDAO.EVENT_SELECT_ALL_PICTURES_WITH_TRIP_ID, new String[] {String.valueOf(idTrip)});
	}
	
}