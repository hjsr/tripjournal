package fr.m2ihm.lnso.tripjournal.activities;

import java.io.FileNotFoundException;
import java.text.DateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.Set;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.provider.ContactsContract.Contacts;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import fr.m2ihm.lnso.tripjournal.R;
import fr.m2ihm.lnso.tripjournal.database.DatabaseHelper;
import fr.m2ihm.lnso.tripjournal.database.EventDAO;
import fr.m2ihm.lnso.tripjournal.database.ParticipantDAO;
import fr.m2ihm.lnso.tripjournal.database.TripDAO;
import fr.m2ihm.lnso.tripjournal.entities.Event;
import fr.m2ihm.lnso.tripjournal.entities.EventTypeEnum;
import fr.m2ihm.lnso.tripjournal.entities.Participant;
import fr.m2ihm.lnso.tripjournal.util.ImageResizer;

public class EventFormActivity extends Activity {

	/* Constants */
	public static final String EXTRA_LATITUDE = "latitude";
	public static final String EXTRA_LONGITUDE = "longitude";
	private static final int PICK_PICTURE = 100;
	private static final int PICK_PLACE = 200;
	private static final int PICK_PARTICIPANT = 300;
	private static final String DIALOG_TAG_CANCEL = "cancelDialog";
	private static final String PICKER_DIALOG_TAG_DATE = "datePickerDialog";
	
	/* Event attribute */
	private Event mEvent; /* Event */
	
	/* Event related views */
	private RatingBar mRatbarEventMark; /* Rating bar event mark */
	private EditText mEditEventName; /* Edit text event name */
	private EditText mEditEventDescription; /* Edit text event description */
	private Button mBtnEventDate; /* Button selection event date */
	private Spinner mSpinEventType; /* Spinner event type */
	private ImageButton mImgbtnEventPlaceDelete; /* Image button delete event place */
	private TextView mTvEventPlaceAlert; /* Text view event place alert */
	private ImageView mImgvEventPicture; /* Image event picture */
	private ImageButton mImgbtnEventPictureDelete; /* Image button delete event picture */
	private LinearLayout mLayEventParticipants; /* Layout event participants */
	private Bitmap mBitmapEventPicture; /* Bitmap event picture */
	private ScrollView mScrlEventForm; /* Scroll view containing the event form */
	private int mScrollPosition; /* Current scroll position */
	
	/* Database and date */
	private DateFormat mDateFormat; /* DateFormat used parse time */
	private Calendar mCalendar; /* Calendar */
	private DatabaseHelper mDatabaseHelper; /* Db helper */
	private Cursor mCursor; /* Cursor */

	/********** Activity methods ***********/
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_event_form);
	
		// Get views and set attributes
		mRatbarEventMark = (RatingBar) findViewById(R.id.ratbar_eventMark);
		mRatbarEventMark.setStepSize(1);
		mEditEventName = (EditText) findViewById(R.id.edit_eventName);
		mEditEventDescription = (EditText) findViewById(R.id.edit_eventDescription);
		mBtnEventDate = (Button) findViewById(R.id.btn_eventDate);
		mSpinEventType = (Spinner) findViewById(R.id.spin_eventType);
		mSpinEventType.setAdapter(ArrayAdapter.createFromResource(this, R.array.types_array, android.R.layout.simple_spinner_dropdown_item));
		mImgbtnEventPlaceDelete = (ImageButton) findViewById(R.id.imgbtn_eventPlaceDelete);
		mTvEventPlaceAlert = (TextView) findViewById(R.id.tv_eventPlaceAlert);
		mImgvEventPicture = (ImageView) findViewById(R.id.imgv_eventPicture);
		mImgbtnEventPictureDelete = (ImageButton) findViewById(R.id.imgbtn_eventPictureDelete);
		mLayEventParticipants = (LinearLayout) findViewById(R.id.lay_eventParticipants);
		mScrlEventForm = (ScrollView) findViewById(R.id.scrl_eventForm);
		mScrollPosition = 0;
		
		// Set the database and date attributes
		mCalendar = Calendar.getInstance();
		mDateFormat = android.text.format.DateFormat.getMediumDateFormat(this);
		mDatabaseHelper = DatabaseHelper.getInstance(this);
		
		// Get the event from the intent
		mEvent = (Event) getIntent().getSerializableExtra(EventActivity.EXTRA_EVENT);
		
		// If the form is called to create a new event,
		// set the date with the start date of the parent trip.
		if (mEvent.getmId() == EventActivity.NEW_EVENT_ID) {
			TripDAO tripDAO = new TripDAO(mDatabaseHelper.getReadableDatabase());
			mCursor = tripDAO.select(mEvent.getmIdTrip());
			mCursor.moveToFirst();
			mEvent.setmDate(mCursor.getLong(mCursor.getColumnIndex(DatabaseHelper.TRIP_START_DATE)));
			mCursor.close();
			
			// Set the views
			mBtnEventDate.setText(mDateFormat.format(new Date(mEvent.getmDate())));
			mImgbtnEventPlaceDelete.setVisibility(View.INVISIBLE);
			mImgbtnEventPictureDelete.setVisibility(View.INVISIBLE);
			mTvEventPlaceAlert.setText(getResources().getString(R.string.no_place_selected));
		}
		
		// If the form is called to edit an event,
		// set the event attributes with the data from database.
		else {
		    // Set the views
		    mRatbarEventMark.setRating(mEvent.getmMark());
			mEditEventName.setText(mEvent.getmName());
			mEditEventDescription.setText(mEvent.getmDescription());
			mBtnEventDate.setText(mDateFormat.format(new Date(mEvent.getmDate())));
			mSpinEventType.setSelection(mEvent.getmType().ordinal());
			
			if (mEvent.getmLatitude() == 0 && mEvent.getmLongitude() == 0) {
				mImgbtnEventPlaceDelete.setVisibility(View.INVISIBLE);
			}
			else {
				mTvEventPlaceAlert.setText(getResources().getString(R.string.place_selected));
			}
			
			// If the event picture attribute is not null, set image view
            if (!mEvent.getmPicture().isEmpty()) {
            	try {
					mBitmapEventPicture = ImageResizer.decodeSampledBitmapFromUri(
							this,
							Uri.parse(mEvent.getmPicture()),
							mImgvEventPicture.getWidth(),
							mImgvEventPicture.getHeight());
					mImgvEventPicture.setImageBitmap(mBitmapEventPicture);
				} catch (FileNotFoundException e) {
					e.printStackTrace();
				}
            }
            // If the event has no picture, set default image view and picture delete button invisible
            else {
            	mImgvEventPicture.setImageResource(android.R.drawable.ic_menu_gallery);
                mImgbtnEventPictureDelete.setVisibility(View.INVISIBLE);
            }
			// Set layout participant with participant attribute of the event
			mLayEventParticipants.removeAllViews();
    		Iterator<Participant> iter = mEvent.getmParticipants().iterator();
    		while (iter.hasNext())
    		{
    			Participant p = iter.next();
    		    View participantView = getLayoutInflater().inflate(R.layout.list_item, null);
    		    participantView.setTag(p);
    		    ((TextView) participantView.findViewById(R.id.tv_eventParticipantName)).setText(p.getmName());
    		    mLayEventParticipants.addView(participantView);
    		}
    		// Set title
		    setTitle(mEvent.getmName());
		}
	}
	
	@Override
	public void onBackPressed() {
		showCancelDialog();
	}

	@Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        
        // Activity gets the result for picking a picture.
        // - set the event picture attribute
        // - set visible the delete picture button
        // - set the image view picture for previsualization
        if (requestCode == EventFormActivity.PICK_PICTURE) {
            if (resultCode == RESULT_OK) {
            	mEvent.setmPicture(data.getDataString());
            	mImgbtnEventPictureDelete.setVisibility(View.VISIBLE);
            	try {
            		mBitmapEventPicture = ImageResizer.decodeSampledBitmapFromUri(
							this,
							Uri.parse(mEvent.getmPicture()),
							mImgvEventPicture.getWidth(),
							mImgvEventPicture.getHeight());
                    mImgvEventPicture.setImageBitmap(mBitmapEventPicture);
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
            }
        }
        
        // Activity gets the result for picking a participant.
        // - add the participant in the event participant attribute (set)
        // - add the participant in the layout containing the participants
        else if (requestCode == EventFormActivity.PICK_PARTICIPANT) {
            if (resultCode == RESULT_OK) {
            	// Add the participant in the participants set of the event
            	mCursor = getContentResolver().query(ContactsContract.Contacts.CONTENT_URI, null, ContactsContract.Contacts._ID + " = ?", new String[] {data.getData().getLastPathSegment()}, null);
            	mCursor.moveToFirst();
            	Set<Participant> setParticipant = mEvent.getmParticipants();
            	setParticipant.add(new Participant(-1, mCursor.getString(mCursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME)), data.getData().getLastPathSegment(), mEvent.getmId()));
            	mEvent.setmParticipants(setParticipant);
            	mCursor.close();
            	
            	// Add the participant in the layout containing the participants
        		mLayEventParticipants.removeAllViews();
        		Iterator<Participant> iter = mEvent.getmParticipants().iterator();
        		while (iter.hasNext())
        		{
        			Participant p = iter.next();
        		    View participantView = getLayoutInflater().inflate(R.layout.list_item, null);
        		    participantView.setTag(p);
        		    ((TextView) participantView.findViewById(R.id.tv_eventParticipantName)).setText(p.getmName());
        		    mLayEventParticipants.addView(participantView);
        		}
            }
        }
        
        // Activity gets the result for picking a place
        else if (requestCode == EventFormActivity.PICK_PLACE) {
            if (resultCode == RESULT_OK) {
            	mEvent.setmLatitude((float) data.getDoubleExtra(EXTRA_LATITUDE, 0));
            	mEvent.setmLongitude((float) data.getDoubleExtra(EXTRA_LONGITUDE, 0));
            	mTvEventPlaceAlert.setText(getResources().getString(R.string.place_selected));
            	mImgbtnEventPlaceDelete.setVisibility(View.VISIBLE);
            }
        }
        
        // Set the scroll back to its previous position
        mEditEventDescription.clearFocus();
        mEditEventName.clearFocus();
        
        mScrlEventForm.post(new Runnable() {
            public void run() {
            	mScrlEventForm.scrollTo(0, mScrollPosition);
            }
        });
        
    }
	
	/********** Button listeners ***********/
	
	/**
	 * Method called when the date view (button) is clicked.
	 * Show a date picker.
	 * 
	 * @param v the clicked view
	 */
	public void showDatePickerDialog(View v) {
		DialogFragment newFragment = new DatePickerFragment();
	    newFragment.show(getFragmentManager(), PICKER_DIALOG_TAG_DATE);
	}
	
	/**
	 * Method called when the cancel view (button) is clicked.
	 * 
	 * @param v the clicked view
	 */
	public void showCancelDialog(View v) {
		showCancelDialog();
	}

	private void showCancelDialog() {
		DialogFragment newFragment = new CancelDialogFragment();
		newFragment.show(getFragmentManager(), DIALOG_TAG_CANCEL);
	}
	
	/**
	 * Method called when the place delete view (image button) is clicked.
	 *  - set to 0 the event latitude and longitude attributes
	 *  - set the delete place view invisible
	 * @param v the clicked view
	 */
	public void deletePlace(View v) {
		mEvent.setmLatitude(0);
		mEvent.setmLongitude(0);
		mTvEventPlaceAlert.setText(getResources().getString(R.string.no_place_selected));
		mImgbtnEventPlaceDelete.setVisibility(View.INVISIBLE);
	}
	
	/**
	 * Method called when the picture delete view (image button) is clicked.
	 *  - set to null the event picture attribute
	 *  - set the delete place view invisible
	 *  - set the image of the previsualization view to default
	 * @param v the clicked view
	 */
	public void deletePicture(View v) {
	    mEvent.setmPicture("");
	    mImgbtnEventPictureDelete.setVisibility(View.INVISIBLE);
	    mImgvEventPicture.setImageResource(android.R.drawable.ic_menu_gallery);
	}
	
	/**
	 * Method called when the participant delete view (image button) is clicked.
	 *  - remove the corresponding participant from the event participant attribute
	 *  - set the participant list view (linear layout)
	 * @param v the clicked view
	 */
	public void deleteParticipant(View v) {
	    LinearLayout parent = (LinearLayout) v.getParent();
		mLayEventParticipants.removeView(parent);
		Set<Participant> setParticipants = mEvent.getmParticipants();
		setParticipants.remove(parent.getTag());
		mEvent.setmParticipants(setParticipants);
	}
	
	/**
	 * Method called when the place selection view (button) is clicked.
	 * 
	 * @param v the clicked view
	 */
	public void startSelectPlaceIntent(View v) {
		// Get the scroll position
		mScrollPosition = mScrlEventForm.getScrollY();
		// Start the activity
		Intent intent = new Intent(this, PlacePickerActivity.class);
		intent.putExtra(EXTRA_LATITUDE, mEvent.getmLatitude());
		intent.putExtra(EXTRA_LONGITUDE, mEvent.getmLongitude());
		startActivityForResult(intent, EventFormActivity.PICK_PLACE);
	}
	
	/**
	 * Method called when the picture selection view (button) is clicked.
	 * 
	 * @param v the clicked view
	 */
	public void startSelectPictureIntent(View v) {
		// Get the scroll position
		mScrollPosition = mScrlEventForm.getScrollY();
		// Start the activity
		Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        startActivityForResult(intent, EventFormActivity.PICK_PICTURE);
	}
	
	/**
	 * Method called when the participant selection view (button) is clicked.
	 * 
	 * @param v the clicked view
	 */
	public void startSelectParticipantIntent(View v) {
		// Get the scroll position
		mScrollPosition = mScrlEventForm.getScrollY();
		// Start the activity
		Intent intent = new Intent(Intent.ACTION_PICK, Contacts.CONTENT_URI);  
	    startActivityForResult(intent, EventFormActivity.PICK_PARTICIPANT);
	}
	
	/**
	 * Method called when the save view (button) is clicked.
	 * 
	 * @param v the clicked view
	 */
	public void confirmEventEdited(View v) {
		// Set event name and description attributes
		mEvent.setmDescription(mEditEventDescription.getText().toString());
		mEvent.setmName(mEditEventName.getText().toString());
		// If the event name or description is empty, show an error message
		if (mEvent.getmName().length() == 0 || mEvent.getmDescription().length() == 0) {
			Toast.makeText(this, getString(R.string.alert_event_fields_empty), Toast.LENGTH_SHORT).show();
		}
		// If the event has attributes correctly set
		else {
			// Set event attributes
			mEvent.setmMark(mRatbarEventMark.getRating());
			mEvent.setmType(EventTypeEnum.values()[mSpinEventType.getSelectedItemPosition()]);
			
			// Insert or update the event
			EventDAO eventDAO = new EventDAO(mDatabaseHelper.getWritableDatabase());
			if (mEvent.getmId() == EventActivity.NEW_EVENT_ID) {
				mEvent.setmId(eventDAO.insert(mEvent)); // The event id is set because we use it to put the participants in the database
			}
			else {
				eventDAO.update(mEvent);
			}
			
			// Put the participants in the database
			ParticipantDAO participantDAO = new ParticipantDAO(mDatabaseHelper.getWritableDatabase());
			participantDAO.deleteAllParticipantForAnEventId(mEvent.getmId());
			for (Participant p : mEvent.getmParticipants()) {
				p.setmIdEvent(mEvent.getmId());
				participantDAO.insert(p);
			}
			
			// Notify the user that the journal has been edited
			Toast.makeText(this, getString(R.string.journal_modified), Toast.LENGTH_SHORT).show();
			setResult(RESULT_OK);
			finish();
		}
	}
	
	/********** Inner classes ***********/
	
	/**
	 * The date picker for the event date.
	 * The minimum date for the date picker is the start date of the parent trip.
	 * The maximum date for the date picker is the end date of the parent trip.
	 */
	public static class DatePickerFragment extends DialogFragment implements DatePickerDialog.OnDateSetListener {
		@Override
		public Dialog onCreateDialog(Bundle savedInstanceState) {
			EventFormActivity activity = ((EventFormActivity) getActivity());
			
			TripDAO tripDAO = new TripDAO(activity.mDatabaseHelper.getReadableDatabase());
			activity.mCursor = tripDAO.select(activity.mEvent.getmIdTrip());
			activity.mCursor.moveToFirst();
			long mStartDateInMs = activity.mCursor.getLong(activity.mCursor.getColumnIndex(DatabaseHelper.TRIP_START_DATE));
			long mEndDateInMs = activity.mCursor.getLong(activity.mCursor.getColumnIndex(DatabaseHelper.TRIP_END_DATE));
			activity.mCursor.close();
			
			activity.mCalendar.setTimeInMillis(activity.mEvent.getmDate());
			DatePickerDialog d = new DatePickerDialog(activity, this, activity.mCalendar.get(Calendar.YEAR), activity.mCalendar.get(Calendar.MONTH), activity.mCalendar.get(Calendar.DAY_OF_MONTH));
			d.getDatePicker().setMinDate(mStartDateInMs);
			d.getDatePicker().setMaxDate(mEndDateInMs);
			return d;
		}
		
		/*
		 * The listener called when the date is changed.
		 */
		public void onDateSet(DatePicker view, int year, int month, int day) {
			EventFormActivity activity = ((EventFormActivity) getActivity());
			activity.mCalendar.set(year, month, day, 0, 0, 0);
			activity.mEvent.setmDate(activity.mCalendar.getTimeInMillis());			
			activity.mBtnEventDate.setText(activity.mDateFormat.format(new Date(activity.mEvent.getmDate())));
		}
	}
	
	/**
	 * The cancel dialog.
	 */
	public static class CancelDialogFragment extends DialogFragment {
	    @Override
	    public Dialog onCreateDialog(Bundle savedInstanceState) {
	        // Use the Builder class for convenient dialog construction
	        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
	        builder.setMessage(getString(R.string.alert_cancel_editing))
	               .setTitle(getString(R.string.action_cancel_editing))
	               .setPositiveButton(getString(R.string.yes), new DialogInterface.OnClickListener() {
	                   public void onClick(DialogInterface dialog, int id) {
	                	   // User validate the cancel action, finish the activity
	                	   getActivity().setResult(RESULT_CANCELED);
	                       getActivity().finish();
	                   }
	               })
	               .setNegativeButton(getString(R.string.no), new DialogInterface.OnClickListener() {
	                   public void onClick(DialogInterface dialog, int id) {
	                       // User cancelled the dialog, no message shown
	                   }
	               });
	        // Create the AlertDialog object and return it
	        return builder.create();
	    }
	}
	
}
