package fr.m2ihm.lnso.tripjournal.activities;

import java.io.FileNotFoundException;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.app.NavUtils;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.SimpleCursorAdapter;
import android.support.v4.widget.SimpleCursorAdapter.ViewBinder;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnInfoWindowClickListener;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;

import fr.m2ihm.lnso.tripjournal.R;
import fr.m2ihm.lnso.tripjournal.database.DatabaseHelper;
import fr.m2ihm.lnso.tripjournal.database.EventDAO;
import fr.m2ihm.lnso.tripjournal.database.LocationDAO;
import fr.m2ihm.lnso.tripjournal.database.ParticipantDAO;
import fr.m2ihm.lnso.tripjournal.entities.Event;
import fr.m2ihm.lnso.tripjournal.entities.EventTypeEnum;
import fr.m2ihm.lnso.tripjournal.entities.LocationEntity;
import fr.m2ihm.lnso.tripjournal.entities.Participant;
import fr.m2ihm.lnso.tripjournal.entities.Trip;
import fr.m2ihm.lnso.tripjournal.util.ImageResizer;

public class EventActivity extends Activity
		implements FragmentManager.OnBackStackChangedListener {

	/* Constants */
	public static final long NEW_EVENT_ID = -1;
	private static final int ADD_OR_EDIT_EVENT_REQUEST = 0;
	public static final String EXTRA_TRIP = "trip";
	public static final String EXTRA_EVENT = "event";
	private static final String FRAG_TAG_EVENT = "event";
	private static final String FRAG_TAG_MAP = "map";
	private static final String TRANSACTION_NAME_EVENT_FRONT = "eventFront";
	private static final String TRANSACTION_NAME_EVENT_BACK = "eventBack";
	private static final String DIALOG_TAG_DELETE = "deleteDialog";

	/* Views */
	private CharSequence mTitle; /* Activity Title */
	private CharSequence mDrawerTitle; /* Navigation drawer title */
	private DrawerLayout mDrawerLayout; /* Navigation drawer wrapper */
	private LinearLayout mDrawer; /* Navigation drawer content layout */
	private ListView mDrawerList; /* Navigation drawer content list */
	private ActionBarDrawerToggle mDrawerToggle; /* Navigation drawer toggle */
	private boolean mShowingBack = false; /* Whether or not we're showing the back of the event (otherwise showing the front) */

	/* Database and entities */
	private DatabaseHelper mDatabaseHelper; /* DB helper */
	private SimpleCursorAdapter mAdapter; /* Adapter for navigation drawer list */
	private Trip mEventTrip; /* Event parent trip */
	private Event mCurrentEvent; /* Current displayed event */

	private Handler mHandler = new Handler(); /* A handler object, used for deferring UI operations */
    

	/********** Activity methods ***********/

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_event);

		// Get the parent trip from the parent activity TripActivity
		mEventTrip = (Trip) getIntent().getSerializableExtra(EXTRA_TRIP);

		// Initialize the DB helper
		mDatabaseHelper = DatabaseHelper.getInstance(this);

		// Set activity and navigation drawer titles and useful views
		mTitle = mDrawerTitle = mEventTrip.getmName();
		mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
		mDrawer = (LinearLayout) findViewById(R.id.left_drawer);
		mDrawerList = (ListView) findViewById(R.id.left_drawer_list);

		/********** Navigation drawer ***********/

		// Override callbacks for drawer open and close events
		// (proper way with the action bar)
		mDrawerToggle = new ActionBarDrawerToggle(
				this,
				mDrawerLayout,
				R.drawable.ic_drawer, /* drawer icon to replace 'Up' caret */
				R.string.drawer_open, /* "open drawer" description */
				R.string.drawer_close /* "close drawer" description */
		) {

			/* Called when a drawer has settled in a completely closed state. */
			public void onDrawerClosed(View view) {
				getActionBar().setTitle(mTitle);
				invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
			}

			/* Called when a drawer has settled in a completely open state. */
			public void onDrawerOpened(View drawerView) {
				getActionBar().setTitle(mDrawerTitle);
				invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
			}
		};
		// And set the ActionBarDrawerToggle as the DrawerListener
		mDrawerLayout.setDrawerListener(mDrawerToggle);

		// Set a custom shadow that overlays the main content 
		// when the drawer opens
		mDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);

		// Allow Up navigation with the app icon in the action bar
		getActionBar().setDisplayHomeAsUpEnabled(true);
		getActionBar().setHomeButtonEnabled(true);
		// But show 'Up' caret instead of the drawer icon
		mDrawerToggle.setDrawerIndicatorEnabled(false);

		/************** Drawer list *************/

		// Retrieve events from the DB and store them into the trip list of events
		SQLiteDatabase db = mDatabaseHelper.getReadableDatabase();
		mEventTrip.setmEvents(getEventsFromDb(db));
		mEventTrip.setmLocations(getLocationsFromDb(db));
		Cursor cursor = new EventDAO(db).selectAllEventsForATripId(mEventTrip.getmId());

		// Initialize the adapter
		String[] fromColumns = {
				DatabaseHelper.EVENT_NAME,
				DatabaseHelper.EVENT_DATE,
				DatabaseHelper.EVENT_PICTURE
		};
		int[] toViews = {
				R.id.item_title,
				R.id.item_details,
				R.id.item_image
		};
		mAdapter = new SimpleCursorAdapter(
				this,
				R.layout.drawer_list_item,
				cursor,
				fromColumns,
				toViews,
				0);
		// Set a custom view binder to format the displayed start date
		mAdapter.setViewBinder(new ViewBinder() {

			@Override
			public boolean setViewValue(View view, Cursor cursor, int columnIndex) {
				if (columnIndex == 4) { // Start date column index
					DateFormat dateFormat = android.text.format.DateFormat.getMediumDateFormat(EventActivity.this);
					((TextView) view).setText(dateFormat.format(cursor.getLong(columnIndex)));
					return true;
				}
				if (columnIndex == 7) { // Picture column index
					Bitmap imageBitmap;
					try {
						imageBitmap = ImageResizer.decodeSampledBitmapFromUri(
								EventActivity.this,
								Uri.parse(cursor.getString(columnIndex)),
								50,
								50);
						((ImageView) view).setImageBitmap(imageBitmap);
					} catch (FileNotFoundException e) {
						e.printStackTrace();
					}
					return true;
				}
				return false;
			}
		});

		// Add a header view as an item to show all events on the map,
		// and set adapter and listener
		mDrawerList.addHeaderView(getLayoutInflater().inflate(R.layout.drawer_list_header, null));
		mDrawerList.setAdapter(mAdapter);
		mDrawerList.setOnItemClickListener(new DrawerItemClickListener());

		/************* Content frame ************/

		if (savedInstanceState == null) {
			// If there is no saved instance state,
			// add a fragment representing the map
			// (if there is saved instance state,
            // a fragment will have already been added to the activity)
			selectItem(0);
		} else {
			mShowingBack = (getFragmentManager().getBackStackEntryCount() > 1);
		}

		// Monitor back stack changes to ensure the action bar shows the appropriate
        // button (either "photo" or "info")
        getFragmentManager().addOnBackStackChangedListener(this);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present
		getMenuInflater().inflate(R.menu.event, menu);
		return true;
	}

	/* Called whenever we call invalidateOptionsMenu() */
	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		// Hide or show action items related to the content view
		// and if the navigation drawer is open or not
		boolean drawerOpen = mDrawerLayout.isDrawerOpen(mDrawer);
		boolean contentEvent = getFragmentManager().findFragmentByTag(FRAG_TAG_EVENT) != null;
		menu.findItem(R.id.action_new_event).setVisible(!contentEvent && !drawerOpen);
		menu.findItem(R.id.action_edit_event).setVisible(contentEvent  && !drawerOpen);
		menu.findItem(R.id.action_discard_event).setVisible(contentEvent && !drawerOpen);
		menu.findItem(R.id.action_flip).setTitle(mShowingBack ? R.string.action_photo : R.string.action_info);
		menu.findItem(R.id.action_flip).setIcon(mShowingBack ? R.drawable.ic_action_photo : R.drawable.ic_action_info);
		menu.findItem(R.id.action_flip).setVisible(contentEvent && !drawerOpen);
		return super.onPrepareOptionsMenu(menu);
	}

	/*
	 * When using the ActionBarDrawerToggle, you must call it during
	 * onPostCreate() and onConfigurationChanged()...
	 */

	@Override
	protected void onPostCreate(Bundle savedInstanceState) {
		super.onPostCreate(savedInstanceState);
		// Sync the toggle state after onRestoreInstanceState has occurred
		mDrawerToggle.syncState();
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
		// Pass any configuration change to the navigation drawer toggle
		mDrawerToggle.onConfigurationChanged(newConfig);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Pass the event to ActionBarDrawerToggle,
		// if it returns true, then it has handled the app icon touch event
		if (mDrawerToggle.onOptionsItemSelected(item)) {
			return true;
		}

		// Handle action buttons
		switch (item.getItemId()) {
	    case android.R.id.home: // Home button
	        NavUtils.navigateUpFromSameTask(this);
	        return true;
		case R.id.action_new_event: // Add a new event button
			startEventFormActivity(false);
			return true;
		case R.id.action_edit_event: // Edit the current event button
			startEventFormActivity(true);
			return true;
		case R.id.action_discard_event: // Delete the current event button
			new DeleteDialogFragment().show(getFragmentManager(), DIALOG_TAG_DELETE);
			return true;
		case R.id.action_flip: // Info or photo button
            flipCard();
            return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == ADD_OR_EDIT_EVENT_REQUEST) {
            if (resultCode == RESULT_OK) {
                // An event was added
            	// Notify adapter data changed by changing the cursor and updating events list
            	SQLiteDatabase db = mDatabaseHelper.getReadableDatabase();
            	mEventTrip.setmEvents(getEventsFromDb(db));
        		Cursor cursor = new EventDAO(db).selectAllEventsForATripId(mEventTrip.getmId());
        		mAdapter.changeCursor(cursor);
        		selectItem(0); // Show the map
            }
        }
	}

	@Override
	public void setTitle(CharSequence title) {
		mTitle = title;
		getActionBar().setTitle(mTitle);
	}

	/* Listen for when a FragmentTransaction is reverted */
	@Override
	public void onBackStackChanged() {
		mShowingBack = (getFragmentManager().getBackStackEntryCount() > 1);
		// If the is no more transaction on the back stack,
		// update the title with the event name
		if (getFragmentManager().getBackStackEntryCount() == 0) {
			setTitle(mEventTrip.getmName());
		}
		invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
	}

	/*********** Private methods ***********/

	/**
	 * Return a list of events from a SQLite DB
	 * @param db The DB
	 * @return A list of events
	 */
	private List<Event> getEventsFromDb(SQLiteDatabase db) {
		Cursor cursor = new EventDAO(db).selectAllEventsForATripId(mEventTrip.getmId());
		List<Event> events = new ArrayList<Event>();
		while(cursor.moveToNext()) {
			Event e = new Event(
					cursor.getLong(cursor.getColumnIndex(DatabaseHelper.EVENT_KEY)),
					cursor.getString(cursor.getColumnIndex(DatabaseHelper.EVENT_NAME)),
					cursor.getLong(cursor.getColumnIndex(DatabaseHelper.EVENT_TRIP_ID)),
					cursor.getString(cursor.getColumnIndex(DatabaseHelper.EVENT_DESCRIPTION)),
					cursor.getLong(cursor.getColumnIndex(DatabaseHelper.EVENT_DATE)),
					Enum.valueOf(EventTypeEnum.class, cursor.getString(cursor.getColumnIndex(DatabaseHelper.EVENT_TYPE))),
					cursor.getFloat(cursor.getColumnIndex(DatabaseHelper.EVENT_MARK)),
					cursor.getString(cursor.getColumnIndex(DatabaseHelper.EVENT_PICTURE)),
					cursor.getFloat(cursor.getColumnIndex(DatabaseHelper.EVENT_LATITUDE)),
					cursor.getFloat(cursor.getColumnIndex(DatabaseHelper.EVENT_LONGITUDE)));

			// Also get the participants for the event
			e.setmParticipants(getParticipantsFromDb(db, e.getmId()));
			events.add(e);
		}
		return events;
	}

	/**
	 * Return a set of participants for an event from a SQLite DB
	 * @param db The DB
	 * @param eventId The event ID
	 * @return A set of participants
	 */
	private Set<Participant> getParticipantsFromDb(SQLiteDatabase db, long eventId) {
		Cursor cursor = new ParticipantDAO(db).selectAllParticipantForAnEventId(eventId);
		Set<Participant> participants = new HashSet<Participant>();
		while(cursor.moveToNext()) {
			participants.add(new Participant(
					cursor.getLong(cursor.getColumnIndex(DatabaseHelper.PARTICIPANT_KEY)),
					cursor.getString(cursor.getColumnIndex(DatabaseHelper.PARTICIPANT_CONTACT_NAME)),
					cursor.getString(cursor.getColumnIndex(DatabaseHelper.PARTICIPANT_CONTACT_ID)),
					cursor.getLong(cursor.getColumnIndex(DatabaseHelper.PARTICIPANT_EVENT_ID))));
		}
		return participants;
	}

	/**
	 * Return a list of locations from a SQLite DB
	 * @param db The DB
	 * @return A list of locations
	 */
	private List<LocationEntity> getLocationsFromDb(SQLiteDatabase db) {
		Cursor cursor = new LocationDAO(db).selectAllLocationsForATripId((mEventTrip.getmId()));
		List<LocationEntity> locations = new ArrayList<LocationEntity>();
		while(cursor.moveToNext()) {
			locations.add(new LocationEntity(
					cursor.getLong(cursor.getColumnIndex(DatabaseHelper.LOCATION_KEY)),
					new LatLng(
							cursor.getFloat(cursor.getColumnIndex(DatabaseHelper.LOCATION_LATITUDE)),
							cursor.getFloat(cursor.getColumnIndex(DatabaseHelper.LOCATION_LONGITUDE))),
					cursor.getLong(cursor.getColumnIndex(DatabaseHelper.LOCATION_TRIP_ID))));
		}
		return locations;
	}

	/**
	 * The click listener for ListView in the navigation drawer
	 */
	private class DrawerItemClickListener implements
			ListView.OnItemClickListener {
		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
			selectItem(position);
		}
	}

	/**
	 * Swaps fragments in the main content view
	 * @param position The position of the item in the list
	 */
	private void selectItem(int position) {
		getFragmentManager().popBackStack(TRANSACTION_NAME_EVENT_FRONT, FragmentManager.POP_BACK_STACK_INCLUSIVE); // Delete the previous transaction from the back stack in order to keep only one at a time
		if (position > 0) { // If an event is selected
			// Update the main content by replacing fragments:
			// 1) Initialize an event fragment
			Fragment eventFragment = new EventFrontFragment();
			Bundle args = new Bundle();
			args.putInt(EventFrontFragment.ARG_EVENT_NUMBER, position - 1);
			eventFragment.setArguments(args);

			// 2) Replace whatever is in the fragment container view with this fragment
			// and add the transaction to the back stack so the user can navigate back
			getFragmentManager().beginTransaction()
								.replace(R.id.content_frame, eventFragment, FRAG_TAG_EVENT)
								.addToBackStack(TRANSACTION_NAME_EVENT_FRONT).commit();
		} else { // If the header is selected
			// Show the map and update the title
			getFragmentManager().beginTransaction()
								.replace(R.id.content_frame, new CustomMapFragment(), FRAG_TAG_MAP)
								.commit();
			setTitle(mEventTrip.getmName());
		}

		// Highlight the selected item
		mDrawerList.setItemChecked(position, true);

		// Close the navigation drawer
		mDrawerLayout.closeDrawer(mDrawer);
	}

	private void flipCard() {
		if (mShowingBack) {
            getFragmentManager().popBackStack();
            return;
        }

        // Flip to the back
        mShowingBack = true;

        // Create and commit a new fragment transaction that adds the fragment for the back of
        // the event, uses custom animations, and is part of the fragment manager's back stack
        getFragmentManager()
                .beginTransaction()

                // Replace the default fragment animations with animator resources representing
                // rotations when switching to the back of the event, as well as animator
                // resources representing rotations when flipping back to the front (e.g. when
                // the system Back button is pressed).
                .setCustomAnimations(
                        R.animator.card_flip_right_in, R.animator.card_flip_right_out,
                        R.animator.card_flip_left_in, R.animator.card_flip_left_out)

                // Replace any fragments currently in the container view with a fragment
                // representing the next page (indicated by the just-incremented currentPage
                // variable)
                .replace(R.id.content_frame, new EventBackFragment())

                // Add this transaction to the back stack, allowing users to press Back
                // to get to the front of the card
                .addToBackStack(TRANSACTION_NAME_EVENT_BACK)

                // Commit the transaction
                .commit();

        // Defer an invalidation of the options menu (on modern devices, the action bar). This
        // can't be done immediately because the transaction may not yet be committed. Commits
        // are asynchronous in that they are posted to the main thread's message loop.
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                invalidateOptionsMenu();
            }
        });
	}

	/**
	 * Delete the current event from the DB
	 * @return > 0 if the event was deleted, -1 otherwise
	 */
	private int deleteCurrentEvent() {
		EventDAO eventDAO = new EventDAO(mDatabaseHelper.getWritableDatabase());
		int deleted = eventDAO.delete(mCurrentEvent.getmId());

		if (deleted > 0) {
			// The event was deleted
			// Notify adapter data changed by changing the cursor and updating events list
			mEventTrip.setmEvents(getEventsFromDb(mDatabaseHelper.getReadableDatabase()));
			Cursor cursor = eventDAO.selectAllEventsForATripId(mEventTrip.getmId());
			mAdapter.changeCursor(cursor);
		}

		// Show the map
		selectItem(0);

		return deleted;
	}

	/**
	 * Listener for the open drawer button
	 * @param view The clicked view
	 */
	public void openDrawer(View view) {
		mDrawerLayout.openDrawer(mDrawer);
	}

	/**
	 * Listener for the add a new event button
	 * @param view The clicked view
	 */
	public void startEventFormActivity(View view) {
		mDrawerLayout.closeDrawer(mDrawer);
		startEventFormActivity(false);
	}

	/**
	 * Start the activity EventFormActivity and pass it an event
	 * (a new event if addition or the current event if edition)
	 * @param edit If the activity is started to edit the current event
	 */
	private void startEventFormActivity(boolean edit) {
		Intent intent = new Intent(this, EventFormActivity.class);
		if (!edit) {
			intent.putExtra(EXTRA_EVENT, new Event(NEW_EVENT_ID, "", mEventTrip.getmId(), "", 0, null, 0, "", 0, 0));
		} else {
			intent.putExtra(EXTRA_EVENT, mCurrentEvent);
		}
		startActivityForResult(intent, ADD_OR_EDIT_EVENT_REQUEST);
	}

	/********** Inner classes ***********/

	/**
	 * Map fragment that shows a map with markers for events
	 */
	public static class CustomMapFragment extends MapFragment {
		private static final int CAMERA_PADDING = 100;
		private Map<String, Integer> markers; /* Map which maps markers ids with events positions */

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			View returnView = super.onCreateView(inflater, container, savedInstanceState);

			GoogleMap map = getMap();
			markers = new HashMap<String, Integer>();

			// Add markers for events to the map
			map.clear(); // Clear all previous markers
			LatLngBounds.Builder builder = new LatLngBounds.Builder();
			boolean builderEmpty = true;
			int placeCounter = 1;
			for (Event e : ((EventActivity) getActivity()).mEventTrip.getmEvents()) {
				float lat = e.getmLatitude();
				float lng = e.getmLongitude();
				if ((lat != 0) && (lng != 0)) {
					// Add the marker only if a place was selected for this event
					Marker marker = map.addMarker(new MarkerOptions()
							.position(new LatLng(lat, lng))
							.title(e.getmName()));
					markers.put(marker.getId(), placeCounter);
					builder.include(new LatLng(lat, lng));
					builderEmpty = false;
				}
				placeCounter++;
			}

			// Add polylines for the trip
			PolylineOptions rectOptions = new PolylineOptions();
			for (LocationEntity ll : ((EventActivity) getActivity()).mEventTrip.getmLocations()) {
				rectOptions.add(ll.getmLocation());
			}
			rectOptions.color(Color.argb(150, 242, 105, 85));
			map.addPolyline(rectOptions);

			// Set the camera to the greatest possible zoom level
			// that includes the bounds only if at least
			// one place was selected for an event
			if (!builderEmpty) {
				DisplayMetrics displayMetrics = new DisplayMetrics();
		        getActivity().getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
				map.moveCamera(CameraUpdateFactory.newLatLngBounds(builder.build(), displayMetrics.widthPixels, displayMetrics.heightPixels, CAMERA_PADDING));
			}

			// Show the corresponding event when clicking on the marker info window
			map.setOnInfoWindowClickListener(new OnInfoWindowClickListener() {

				@Override
				public void onInfoWindowClick(Marker marker) {
					((EventActivity) getActivity()).selectItem(markers.get(marker.getId()));
				}
			});

			return returnView;
		}
	}

	/**
	 * Fragment that appears in the "content_frame", shows an event
	 * and represents the front of the card
	 */
	public static class EventFrontFragment extends Fragment {
		public static final String ARG_EVENT_NUMBER = "event_number";

		public EventFrontFragment() {
			// Empty constructor required for fragment subclasses
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			Event event = ((EventActivity) getActivity()).mEventTrip.getmEvents().get(getArguments().getInt(ARG_EVENT_NUMBER));

			// Update image
			ImageView rootView = (ImageView) inflater.inflate(R.layout.fragment_event_front, container, false);
			if (!event.getmPicture().isEmpty()) {
				// Create bitmap
				Bitmap imageBitmap;
				try {
					DisplayMetrics displayMetrics = new DisplayMetrics();
					getActivity().getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
					imageBitmap = ImageResizer.decodeSampledBitmapFromUri(
							getActivity(),
							Uri.parse(event.getmPicture()),
							displayMetrics.widthPixels,
							displayMetrics.heightPixels);

					// Set image
					rootView.setImageBitmap(imageBitmap);
				} catch (FileNotFoundException e) {
					e.printStackTrace();
				}
			}

			// Set the current event and the title
			((EventActivity) getActivity()).mCurrentEvent = event;
			getActivity().setTitle(event.getmName());
			return rootView;
		}
	}

    /**
     * A fragment representing the back of the card.
     */
    public static class EventBackFragment extends Fragment {

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                Bundle savedInstanceState) {
        	Event event = ((EventActivity) getActivity()).mCurrentEvent;
            // Update views:
        	View rootView = inflater.inflate(R.layout.fragment_event_back, container, false);
			DateFormat dateFormat = android.text.format.DateFormat.getMediumDateFormat(getActivity());

			// -> Date and description
			((TextView) rootView.findViewById(R.id.event_date)).setText(dateFormat.format(event.getmDate()));
			((TextView) rootView.findViewById(R.id.event_description)).setText(event.getmDescription());

			// -> Participants
			boolean first = true;
			String participants = getResources().getString(R.string.with) + " ";
			for (Participant p : event.getmParticipants()) {
				if (!first) {
					participants += ", ";
				} else {
					first = false;
				}
				participants += p.getmName();
			}
			((TextView) rootView.findViewById(R.id.event_participants)).setText(participants);

			// -> Type
			int imageRes = 0;
			switch (event.getmType()) {
			case SEE:
				imageRes = R.drawable.type_see;
				break;
			case DO:
				imageRes = R.drawable.type_do;
				break;
			case EAT:
				imageRes = R.drawable.type_eat;
				break;
			case DRINK:
				imageRes = R.drawable.type_drink;
				break;
			case BUY:
				imageRes = R.drawable.type_buy;
				break;
			case SLEEP:
				imageRes = R.drawable.type_sleep;
				break;
			case OTHER:
				imageRes = R.drawable.type_other;
				break;
			}
			((ImageView) rootView.findViewById(R.id.event_type)).setImageBitmap(BitmapFactory.decodeResource(getResources(), imageRes));

			// -> Rating bar
			((RatingBar) rootView.findViewById(R.id.event_mark)).setRating(event.getmMark());
			return rootView;
        }
    }

	/**
	 * Dialog fragment that confirms event deletion
	 */
	public static class DeleteDialogFragment extends DialogFragment {

		@Override
		public Dialog onCreateDialog(Bundle savedInstanceState) {
			AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
			builder.setMessage(getString(R.string.alert_lose_event))
					.setTitle(getString(R.string.action_discard_event))
					.setPositiveButton(getString(R.string.yes),
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog, int id) {
									if (((EventActivity) getActivity()).deleteCurrentEvent() > 0) {
										Toast.makeText(getActivity(), getString(R.string.event_deleted), Toast.LENGTH_SHORT).show();
									} else {
										Toast.makeText(getActivity(), getString(R.string.error), Toast.LENGTH_SHORT).show();
									}
								}
							})
					.setNegativeButton(getString(R.string.no),
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog, int id) {
									// User cancelled the dialog
									// Do nothing
								}
							});
			// Create the AlertDialog object and return it
			return builder.create();
		}
	}

}
