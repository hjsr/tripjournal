package fr.m2ihm.lnso.tripjournal.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.ActionMode;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnMapClickListener;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import fr.m2ihm.lnso.tripjournal.R;

public class PlacePickerActivity extends Activity {
	private static final int CAMERA_ZOOM = 7;

	private GoogleMap mMap;
	private Marker mCurrentMarker;

	protected ActionMode mActionMode;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_place_picker);

		mMap = ((MapFragment) getFragmentManager().findFragmentById(R.id.map)).getMap();
		mMap.setMyLocationEnabled(true);

		// Center map and zoom to the previous selected place if any
		float lat = getIntent().getFloatExtra(EventFormActivity.EXTRA_LATITUDE, 0);
		float lng = getIntent().getFloatExtra(EventFormActivity.EXTRA_LONGITUDE, 0);
		if ((lat != 0) && (lng != 0)) {
			// Start the CAB using the ActionMode.Callback defined above
			mActionMode = startActionMode(mActionModeCallback);
			LatLng previousPlace = new LatLng(lat, lng);
			mCurrentMarker = mMap.addMarker(new MarkerOptions().position(previousPlace));
			mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(previousPlace, CAMERA_ZOOM));
		}

		// Pick the place when clicking on the map
		mMap.setOnMapClickListener(new OnMapClickListener() {

			@Override
			public void onMapClick(LatLng ll) {
				if (mCurrentMarker != null) {
					mCurrentMarker.remove();
				} else {
					// Start the CAB using the ActionMode.Callback defined above
					mActionMode = startActionMode(mActionModeCallback);
				}
				mCurrentMarker = mMap.addMarker(new MarkerOptions().position(ll));
				mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(ll, CAMERA_ZOOM));
			}
		});
	}

	private ActionMode.Callback mActionModeCallback = new ActionMode.Callback() {

		// Called when the action mode is created; startActionMode() was called
		@Override
		public boolean onCreateActionMode(ActionMode mode, Menu menu) {
			// Inflate a menu resource providing context menu items
			MenuInflater inflater = mode.getMenuInflater();
			inflater.inflate(R.menu.context_menu, menu);
			return true;
		}

		// Called each time the action mode is shown.
		// Always called after onCreateActionMode, but
		// may be called multiple times if the mode is invalidated.
		@Override
		public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
			return false; // Return false if nothing is done
		}

		// Called when the user selects a contextual menu item
		@Override
		public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
			switch (item.getItemId()) {
			case R.id.action_remove_place:
				mCurrentMarker.remove();
				mCurrentMarker = null;
				mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(0, 0), 0));
				mode.finish(); // Action picked, so close the CAB
				return true;
			default:
				return false;
			}
		}

		// Called when the user exits the action mode
		@Override
		public void onDestroyActionMode(ActionMode mode) {
			mActionMode = null;
			if (mCurrentMarker != null) {
				Intent returnData = new Intent();
				returnData.putExtra(EventFormActivity.EXTRA_LATITUDE, mCurrentMarker.getPosition().latitude);
				returnData.putExtra(EventFormActivity.EXTRA_LONGITUDE, mCurrentMarker.getPosition().longitude);
				setResult(RESULT_OK, returnData);
				finish();
			}
		}
	};

}
