package fr.m2ihm.lnso.tripjournal.activities;

import java.text.DateFormat;
import java.util.Calendar;
import java.util.Date;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.NumberPicker;
import android.widget.NumberPicker.OnValueChangeListener;
import android.widget.Toast;
import fr.m2ihm.lnso.tripjournal.R;
import fr.m2ihm.lnso.tripjournal.database.DatabaseHelper;
import fr.m2ihm.lnso.tripjournal.database.TripDAO;
import fr.m2ihm.lnso.tripjournal.entities.Trip;

public class TripFormActivity extends Activity {

	/* Constants */
	private static final int DURATION_MAX = 365;
	private static final String DIALOG_TAG_CANCEL = "cancelDialog";
	private static final String DATE_PICKER_DIALOG_TAG_END = "endDatePickerDialog";
	private static final String DATE_PICKER_DIALOG_TAG_START = "startDatePickerDialog";
	
	/* Trip attribute */
	private Trip mTrip; /* Trip */
	
	/* Trip related views */
	private CharSequence mTitle; /* Activity Title */
	private Button mBtnTripStartDate; /* Button selection trip start date */
	private Button mBtnTripEndDate; /* Button selection trip end date */
	private EditText mEditTripName; /* Edit text trip name */
	private NumberPicker mNbPicker; /* Number picker trip duration in days */
	
	/* Database and date */
	private DateFormat mDateFormat; /* DateFormat used parse time */
	private Calendar mCalendar; /* Calendar */
	private DatabaseHelper mDatabaseHelper; /* Db helper */
	
	/********** Activity methods ***********/
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_trip_form);
		
		// Get views and set attributes
		mTitle = getTitle();
		mBtnTripStartDate = (Button) findViewById(R.id.btn_tripStartDate);
		mBtnTripEndDate = (Button) findViewById(R.id.btn_tripEndDate);
		mEditTripName = (EditText) findViewById(R.id.edit_tripName);
		mNbPicker = (NumberPicker) findViewById(R.id.nbPicker_tripDuration);
		mNbPicker.setMinValue(0);
		mNbPicker.setMaxValue(DURATION_MAX);
		
		// Set the database and date attributes
		mDateFormat = android.text.format.DateFormat.getMediumDateFormat(this);
		mCalendar = Calendar.getInstance();
		mDatabaseHelper = DatabaseHelper.getInstance(this);
		
		// Get the trip from the intent
		mTrip = (Trip) getIntent().getSerializableExtra(TripActivity.EXTRA_TRIP);

		// If the form is called to create a new trip,
		// set the start and end dates with now date.
		if (mTrip.getmId() == TripActivity.NEW_TRIP_ID) {
			mCalendar.set(mCalendar.get(Calendar.YEAR), mCalendar.get(Calendar.MONTH), mCalendar.get(Calendar.DAY_OF_MONTH), 0, 0, 0);
			mTrip.setmStartDate(mCalendar.getTimeInMillis());
			mTrip.setmEndDate(mCalendar.getTimeInMillis());

			// Set the views
		    mNbPicker.setValue(0);
			mBtnTripStartDate.setText(mDateFormat.format(new Date(mTrip.getmStartDate())));
		    mBtnTripEndDate.setText(mDateFormat.format(new Date(mTrip.getmEndDate())));
		}
		
		// If the form is called to edit a trip,
		// set the trip attributes with the data from database.
		else {
			// Set the views
			mNbPicker.setValue(Math.round(((float)(mTrip.getmEndDate() - mTrip.getmStartDate())) / 1000.0f / 60.0f / 60.0f / 24.0f));
			mEditTripName.setText(mTrip.getmName());
			mBtnTripStartDate.setText(mDateFormat.format(new Date(mTrip.getmStartDate())));
		    mBtnTripEndDate.setText(mDateFormat.format(new Date(mTrip.getmEndDate())));
		    
		    // Set title
		    setTitle(mTrip.getmName());
		}
		
		// When the value of the number picker is changed
		// we change the end date to be = start date + number picker value
		mNbPicker.setOnValueChangedListener(new OnValueChangeListener() {
			@Override
			public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
				// Set the end date with start date + number picker value
				mCalendar.setTimeInMillis(mTrip.getmStartDate());
				mCalendar.add(Calendar.DATE, newVal);
				mTrip.setmEndDate(mCalendar.getTimeInMillis());
				
				// Update the views
			    mBtnTripStartDate.setText(mDateFormat.format(new Date(mTrip.getmStartDate())));
			    mBtnTripEndDate.setText(mDateFormat.format(new Date(mTrip.getmEndDate())));
			}
		});
	}

	@Override
	public void onBackPressed() {
		showCancelDialog();
	}

	@Override
	public void setTitle(CharSequence title) {
		mTitle = title;
		getActionBar().setTitle(mTitle);
	}

	/********** Button listeners ***********/

	/**
	 * Method called when the start date view (button) is clicked.
	 * Show a date picker.
	 * 
	 * @param v the clicked view
	 */
	public void showDatePickerDialogStartDate(View v) {
		DialogFragment newFragment = new DatePickerFragmentStartDate();
	    newFragment.show(getFragmentManager(), DATE_PICKER_DIALOG_TAG_START);
	}
	
	/**
	 * Method called when the end date view (button) is clicked.
	 * Show a date picker.
	 * 
	 * @param v the clicked view
	 */
	public void showDatePickerDialogEndDate(View v) {
		DialogFragment newFragment = new DatePickerFragmentEndDate();
	    newFragment.show(getFragmentManager(), DATE_PICKER_DIALOG_TAG_END);
	}
	
	/**
	 * Method called when the cancel view (button) is clicked.
	 * 
	 * @param v the clicked view
	 */
	public void showCancelDialog(View v) {
		showCancelDialog();
	}

	private void showCancelDialog() {
		DialogFragment newFragment = new CancelDialogFragment();
		newFragment.show(getFragmentManager(), DIALOG_TAG_CANCEL);
	}
	
	/**
	 * Method called when the save view (button) is clicked.
	 * 
	 * @param v the clicked view
	 */
	public void confirmTripEdited(View v) {
		// Set trip name attribute
		mTrip.setmName(mEditTripName.getText().toString());
		// If the trip name is empty, show an error message
		if (mTrip.getmName().length() == 0) {
			Toast.makeText(this, getString(R.string.alert_trip_name_empty), Toast.LENGTH_SHORT).show();
		}
		// If the trip has attributes correctly set
		else {
			// Insert or update the trip
			TripDAO tripDAO = new TripDAO(mDatabaseHelper.getWritableDatabase());
			if (mTrip.getmId() == TripActivity.NEW_TRIP_ID) {
				mTrip.setmId(tripDAO.insert(mTrip)); // It is not necessary to set the trip id
			}
			else {
				tripDAO.update(mTrip);
			}
			
			// Notify the user that the journal has been edited
			Toast.makeText(this, getString(R.string.journal_modified), Toast.LENGTH_SHORT).show();
			setResult(RESULT_OK);
			finish();
		}
	}
	
	/********** Inner classes ***********/
	
	/**
	 * The date picker for the start date.
	 * There are no restriction on the start date.
	 */
	public static class DatePickerFragmentStartDate extends DialogFragment implements DatePickerDialog.OnDateSetListener {
		@Override
		public Dialog onCreateDialog(Bundle savedInstanceState) {
			// Set the default date with the start date
			TripFormActivity activity = ((TripFormActivity) getActivity());
			activity.mCalendar.setTimeInMillis(activity.mTrip.getmStartDate());
			return new DatePickerDialog(activity, this, activity.mCalendar.get(Calendar.YEAR), activity.mCalendar.get(Calendar.MONTH), activity.mCalendar.get(Calendar.DAY_OF_MONTH));
		}
		
		/*
		 * The listener called when the date is changed.
		 * Set the start and end dates and update the views.
		 */
		public void onDateSet(DatePicker view, int year, int month, int day) {
			TripFormActivity activity = ((TripFormActivity) getActivity());
			activity.mCalendar.set(year, month, day, 0, 0, 0);
			activity.mTrip.setmStartDate(activity.mCalendar.getTimeInMillis());
			activity.mCalendar.add(Calendar.DATE, activity.mNbPicker.getValue());
			activity.mTrip.setmEndDate(activity.mCalendar.getTimeInMillis());
			
			activity.mBtnTripStartDate.setText(activity.mDateFormat.format(new Date(activity.mTrip.getmStartDate())));
			activity.mBtnTripEndDate.setText(activity.mDateFormat.format(new Date(activity.mTrip.getmEndDate())));
		}
	}
	
	/**
	 * The date picker for the end date.
	 * The minimum date for the date picker is the start date.
	 * The maximum date for the date picker is the start date + DURATION_MAX.
	 */
	public static class DatePickerFragmentEndDate extends DialogFragment implements DatePickerDialog.OnDateSetListener {
		@Override
		public Dialog onCreateDialog(Bundle savedInstanceState) {
			TripFormActivity activity = ((TripFormActivity) getActivity());
			activity.mCalendar.setTimeInMillis(activity.mTrip.getmEndDate());
			
			// Set the default date with the end date
			// and add the restrictions
			DatePickerDialog d = new DatePickerDialog(activity, this, activity.mCalendar.get(Calendar.YEAR), activity.mCalendar.get(Calendar.MONTH), activity.mCalendar.get(Calendar.DAY_OF_MONTH));
			d.getDatePicker().setMinDate(activity.mTrip.getmStartDate());
			activity.mCalendar.setTimeInMillis(activity.mTrip.getmStartDate());
			activity.mCalendar.add(Calendar.DATE, DURATION_MAX);
			d.getDatePicker().setMaxDate(activity.mCalendar.getTimeInMillis());
			return d;
		}		
		
		/*
		 * The listener called when the date is changed.
		 * Set the start and end dates and update the views.
		 */
		public void onDateSet(DatePicker view, int year, int month, int day) {
			TripFormActivity activity = ((TripFormActivity) getActivity());
			activity.mCalendar.set(year, month, day, 0, 0, 0);
			activity.mTrip.setmEndDate(activity.mCalendar.getTimeInMillis());
			
			activity.mNbPicker.setValue(Math.round((activity.mTrip.getmEndDate() - activity.mTrip.getmStartDate()) / 1000 / 60 / 60 / 24));
			activity.mBtnTripStartDate.setText(activity.mDateFormat.format(new Date(activity.mTrip.getmStartDate())));
			activity.mBtnTripEndDate.setText(activity.mDateFormat.format(new Date(activity.mTrip.getmEndDate())));
		}
	}
	
	/**
	 * The cancel dialog.
	 */
	public static class CancelDialogFragment extends DialogFragment {
	    @Override
	    public Dialog onCreateDialog(Bundle savedInstanceState) {
	        // Use the Builder class for convenient dialog construction
	        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
	        builder.setMessage(getString(R.string.alert_cancel_editing))
	               .setTitle(getString(R.string.action_cancel_editing))
	               .setPositiveButton(getString(R.string.yes), new DialogInterface.OnClickListener() {
	                   public void onClick(DialogInterface dialog, int id) {
	                	   // User validate the cancel action, finish the activity
	                	   getActivity().setResult(RESULT_CANCELED);
	                       getActivity().finish();
	                   }
	               })
	               .setNegativeButton(getString(R.string.no), new DialogInterface.OnClickListener() {
	                   public void onClick(DialogInterface dialog, int id) {
	                       // User cancelled the dialog, no message shown
	                   }
	               });
	        // Create the AlertDialog object and return it
	        return builder.create();
	    }
	}

}
