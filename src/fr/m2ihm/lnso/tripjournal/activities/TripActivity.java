package fr.m2ihm.lnso.tripjournal.activities;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Configuration;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.SimpleCursorAdapter;
import android.support.v4.widget.SimpleCursorAdapter.ViewBinder;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnInfoWindowClickListener;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import fr.m2ihm.lnso.tripjournal.R;
import fr.m2ihm.lnso.tripjournal.database.DatabaseHelper;
import fr.m2ihm.lnso.tripjournal.database.EventDAO;
import fr.m2ihm.lnso.tripjournal.database.TripDAO;
import fr.m2ihm.lnso.tripjournal.entities.Trip;
import fr.m2ihm.lnso.tripjournal.services.LocationTrackerService;
import fr.m2ihm.lnso.tripjournal.util.ImageAdapter;

public class TripActivity extends Activity
		implements FragmentManager.OnBackStackChangedListener {

	/* Constants */
	public static final long NEW_TRIP_ID = -1;
	public static final String EXTRA_TRIP = "trip";
	public static final String EXTRA_TRIP_ID = "trip_id";
	private static final int ADD_OR_EDIT_TRIP_REQUEST = 0;
	private static final String FRAG_TAG_TRIP = "trip";
	private static final String FRAG_TAG_MAP = "map";
	private static final String DIALOG_TAG_DELETE = "deleteDialog";

	/* Views */
	private CharSequence mTitle; /* Activity Title */
	private CharSequence mDrawerTitle; /* Navigation drawer title */
	private DrawerLayout mDrawerLayout; /* Navigation drawer wrapper */
	private LinearLayout mDrawer; /* Navigation drawer content layout */
	private ListView mDrawerList; /* Navigation drawer content list */
	private ActionBarDrawerToggle mDrawerToggle; /* Navigation drawer toggle */

	/* Database and entities */
	private DatabaseHelper mDatabaseHelper; /* DB helper */
	private SimpleCursorAdapter mAdapter; /* Adapter for navigation drawer list */
	private List<Trip> mTrips; /* List of all trips */
	private Map<Long, List<LatLng>> mEventsPlaces; /* Map of all trips events places */
	private Trip mCurrentTrip; /* Current displayed trip */

	/********** Activity methods ***********/

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_trip);

		// Initialize the DB helper
		mDatabaseHelper = DatabaseHelper.getInstance(this);

		// Set activity and navigation drawer titles and useful views
		mTitle = mDrawerTitle = getTitle();
		mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
		mDrawer = (LinearLayout) findViewById(R.id.left_drawer);
		mDrawerList = (ListView) findViewById(R.id.left_drawer_list);

		/********** Navigation drawer ***********/

		// Override callbacks for drawer open and close events
		// (proper way with the action bar)
		mDrawerToggle = new ActionBarDrawerToggle(
				this,
				mDrawerLayout,
				R.drawable.ic_drawer, /* drawer icon to replace 'Up' caret */
				R.string.drawer_open, /* "open drawer" description */
				R.string.drawer_close /* "close drawer" description */
		) {

			/* Called when a drawer has settled in a completely closed state. */
			public void onDrawerClosed(View view) {
				getActionBar().setTitle(mTitle);
				invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
			}

			/* Called when a drawer has settled in a completely open state. */
			public void onDrawerOpened(View drawerView) {
				getActionBar().setTitle(mDrawerTitle);
				invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
			}
		};
		// And set the ActionBarDrawerToggle as the DrawerListener
		mDrawerLayout.setDrawerListener(mDrawerToggle);

		// Set a custom shadow that overlays the main content 
		// when the drawer opens
		mDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);

		/************** Drawer list *************/

		// Retrieve trips and events places from the DB
		// and store them into a list of events places and a list of trips
		SQLiteDatabase db = mDatabaseHelper.getReadableDatabase();
		mEventsPlaces = new HashMap<Long, List<LatLng>>(getEventsPlacesFromDb(db));
		mTrips = new ArrayList<Trip>(getTripsFromDb(db));
		Cursor cursor = new TripDAO(db).selectAllTrips();

		// Initialize the adapter
		String[] fromColumns = {
				DatabaseHelper.TRIP_NAME,
				DatabaseHelper.TRIP_START_DATE
		};
		int[] toViews = {
				R.id.item_title,
				R.id.item_details
		};
		mAdapter = new SimpleCursorAdapter(
				this,
				R.layout.drawer_list_item,
				cursor,
				fromColumns,
				toViews,
				0);
		// Set a custom view binder to format the displayed start date
		// and to display the duration
		mAdapter.setViewBinder(new ViewBinder() {

			@Override
			public boolean setViewValue(View view, Cursor cursor, int columnIndex) {
				if (columnIndex == 2) { // Start date column index
					DateFormat dateFormat = android.text.format.DateFormat.getMediumDateFormat(TripActivity.this);
					long duration = (cursor.getLong(3) - cursor.getLong(2)) / (1000 * 60 * 60 * 24);
					((TextView) view).setText(
							dateFormat.format(cursor.getLong(columnIndex)) + ", "
							+ duration + " "
							+ getResources().getString(R.string.days).toLowerCase());
					return true;
				}
				return false;
			}
		});

		// Add a header view as an item to show all trips on the map,
		// and set adapter and listener
		mDrawerList.addHeaderView(getLayoutInflater().inflate(R.layout.drawer_list_header, null));
		mDrawerList.setAdapter(mAdapter);
		mDrawerList.setOnItemClickListener(new DrawerItemClickListener());

		/************* Content frame ************/

		if (savedInstanceState == null) {
			// If there is no saved instance state,
			// add a fragment representing the map
			// (if there is saved instance state,
            // a fragment will have already been added to the activity)
			selectItem(0);
		}

		// Monitor back stack changes to ensure the action bar shows the
		// appropriate button (either "photo" or "info")
		getFragmentManager().addOnBackStackChangedListener(this);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present
		getMenuInflater().inflate(R.menu.trip, menu);
		return true;
	}

	/* Called whenever we call invalidateOptionsMenu() */
	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		// Hide or show action items related to the content view
		// and if the navigation drawer is open or not
		boolean drawerOpen = mDrawerLayout.isDrawerOpen(mDrawer);
		boolean contentTrip = getFragmentManager().findFragmentByTag(FRAG_TAG_TRIP) != null;
		menu.findItem(R.id.action_new_trip).setVisible(!contentTrip && !drawerOpen);
		menu.findItem(R.id.action_edit_trip).setVisible(contentTrip && !drawerOpen);
		menu.findItem(R.id.action_discard_trip).setVisible(contentTrip && !drawerOpen);
		menu.findItem(R.id.action_info).setVisible(contentTrip && !drawerOpen);
		return super.onPrepareOptionsMenu(menu);
	}

	/*
	 * When using the ActionBarDrawerToggle, you must call it during
	 * onPostCreate() and onConfigurationChanged()...
	 */

	@Override
	protected void onPostCreate(Bundle savedInstanceState) {
		super.onPostCreate(savedInstanceState);
		// Sync the toggle state after onRestoreInstanceState has occurred
		mDrawerToggle.syncState();
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
		// Pass any configuration change to the navigation drawer toggle
		mDrawerToggle.onConfigurationChanged(newConfig);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Pass the event to ActionBarDrawerToggle,
		// if it returns true, then it has handled the app icon touch event
		if (mDrawerToggle.onOptionsItemSelected(item)) {
			return true;
		}

		// Handle action buttons
		switch (item.getItemId()) {
		case R.id.action_new_trip: // Add a new trip button
			startTripFormActivity(false);
			return true;
		case R.id.action_edit_trip: // Edit the current trip button
			startTripFormActivity(true);
			return true;
		case R.id.action_discard_trip: // Delete the current trip button
			new DeleteDialogFragment().show(getFragmentManager(), DIALOG_TAG_DELETE);
			return true;
		case R.id.action_info: // Info button
			startEventActivity();
            return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == ADD_OR_EDIT_TRIP_REQUEST) {
            if (resultCode == RESULT_OK) {
                // An trip was added
            	// Notify adapter data changed by changing the cursor and updating trips list
            	SQLiteDatabase db = mDatabaseHelper.getReadableDatabase();
        		mTrips = new ArrayList<Trip>(getTripsFromDb(db));
        		Cursor cursor = new TripDAO(db).selectAllTrips();
        		mAdapter.changeCursor(cursor);
        		selectItem(0); // Show the map
            }
        }
	}

	@Override
	public void setTitle(CharSequence title) {
		mTitle = title;
		getActionBar().setTitle(mTitle);
	}

	@Override
	protected void onResume() {
		super.onResume();
		registerReceiver(onBroadcast, new IntentFilter(LocationTrackerService.ACTION_CONNECTION_STATE));
	}

	@Override
	protected void onPause() {
		super.onPause();
		unregisterReceiver(onBroadcast);
	}

	/* Listen for when a FragmentTransaction is reverted */
	@Override
	public void onBackStackChanged() {
		// If the is no more transaction on the back stack,
		// update the title with the app name
		if (getFragmentManager().getBackStackEntryCount() == 0) {
			setTitle(getTitle());
		}
		invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
	}

	/*********** Private methods ***********/

	/**
	 * Return a list of trips from a SQLite DB
	 * @param db The DB
	 * @return A list of trips
	 */
	private List<Trip> getTripsFromDb(SQLiteDatabase db) {
		Cursor cursor = new TripDAO(db).selectAllTrips();
		List<Trip> trips = new ArrayList<Trip>();
		while(cursor.moveToNext()) {
			trips.add(new Trip(
					cursor.getLong(cursor.getColumnIndex(DatabaseHelper.TRIP_KEY)),
					cursor.getString(cursor.getColumnIndex(DatabaseHelper.TRIP_NAME)),
					cursor.getLong(cursor.getColumnIndex(DatabaseHelper.TRIP_START_DATE)),
					cursor.getLong(cursor.getColumnIndex(DatabaseHelper.TRIP_END_DATE))));
		}
		return trips;
	}

	/**
	 * Return a map of locations with their marker identifier from a SQLite DB
	 * @param db The DB
	 * @return A map of IDs and locations
	 */
	private Map<Long, List<LatLng>> getEventsPlacesFromDb(SQLiteDatabase db) {
		Cursor cursor = new EventDAO(db).selectAllEventsPlacesWithTripId();
		Map<Long, List<LatLng>> eventsPlaces = new HashMap<Long, List<LatLng>>();
		while (cursor.moveToNext()) {
			long tripKey = cursor.getLong(cursor.getColumnIndex(DatabaseHelper.EVENT_TRIP_ID));
			if (!eventsPlaces.containsKey(tripKey)) {
				eventsPlaces.put(tripKey, new ArrayList<LatLng>());
			}
			eventsPlaces.get(tripKey).add(new LatLng(
					cursor.getFloat(cursor.getColumnIndex(DatabaseHelper.EVENT_LATITUDE)),
					cursor.getFloat(cursor.getColumnIndex(DatabaseHelper.EVENT_LONGITUDE))));
		}
		return eventsPlaces;
	}

	/**
	 * Return an array of events images uris for a trip from a SQLite DB
	 * @param db The DB
	 * @param tripId The trip ID
	 * @return An array of images uris
	 */
	private Uri[] getEventsImagesFromDb(SQLiteDatabase db, long tripId) {
		Cursor cursor = new EventDAO(db).selectAllEventsPicturesWithTripId(tripId);
		List<Uri> imagesUris = new ArrayList<Uri>();
		while(cursor.moveToNext()) {
			String stringUri = cursor.getString(cursor.getColumnIndex(DatabaseHelper.EVENT_PICTURE));
			if (!stringUri.isEmpty()) {
				imagesUris.add(Uri.parse(stringUri));
			}
		}
		return (Uri[]) imagesUris.toArray(new Uri[imagesUris.size()]);
	}

	/**
	 * The click listener for ListView in the navigation drawer
	 */
	private class DrawerItemClickListener implements
			ListView.OnItemClickListener {
		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
			selectItem(position);
		}
	}

	/**
	 * Swaps fragments in the main content view
	 * @param position The position of the item in the list
	 */
	private void selectItem(int position) {
		getFragmentManager().popBackStack(); // Delete the previous transaction from the back stack in order to keep only one at a time
		if (position > 0) { // If a trip is selected
			// Update the main content by replacing fragments:
			// 1) Initialize a trip fragment
			Fragment tripFragment = new TripFragment();
			Bundle args = new Bundle();
			args.putInt(TripFragment.ARG_TRIP_NUMBER, position - 1);
			tripFragment.setArguments(args);

			// 2) Replace whatever is in the fragment container view with this fragment
			// and add the transaction to the back stack so the user can navigate back
			getFragmentManager().beginTransaction()
								.replace(R.id.content_frame, tripFragment, FRAG_TAG_TRIP)
								.addToBackStack(null).commit();
		} else { // If the header is selected
			// Show the map and update the title
			getFragmentManager().beginTransaction()
								.replace(R.id.content_frame, new CustomMapFragment(), FRAG_TAG_MAP)
								.commit();
			setTitle(getTitle());
		}


		// Highlight the selected item
		mDrawerList.setItemChecked(position, true);

		// Close the navigation drawer
		mDrawerLayout.closeDrawer(mDrawer);
	}

	/**
	 * Delete the current trip from the DB
	 * @return > 0 if the trip was deleted, -1 otherwise
	 */
	private int deleteCurrentTrip() {
		TripDAO tripDAO = new TripDAO(mDatabaseHelper.getWritableDatabase());
		int deleted = tripDAO.delete(mCurrentTrip.getmId());

		if (deleted > 0) {
			// The trip was deleted
			// Notify adapter data changed by changing the cursor and updating trips list
			mTrips.clear();
			mTrips.addAll(getTripsFromDb(mDatabaseHelper.getReadableDatabase()));
			Cursor cursor = tripDAO.selectAllTrips();
			mAdapter.changeCursor(cursor);
		}

		// Show the map
		selectItem(0);

		return deleted;
	}

	/**
	 * Listener for the open drawer button
	 * @param view The clicked view
	 */
	public void openDrawer(View view) {
		mDrawerLayout.openDrawer(mDrawer);
	}

	/**
	 * Listener for the add a new trip button
	 * @param view The clicked view
	 */
	public void startTripFormActivity(View view) {
		mDrawerLayout.closeDrawer(mDrawer);
		startTripFormActivity(false);
	}

	/**
	 * Start the activity TripFormActivity and pass it a trip
	 * (a new trip if addition or the current trip if edition)
	 * @param edit If the activity is started to edit the current trip
	 */
	private void startTripFormActivity(boolean edit) {
		Intent intent = new Intent(this, TripFormActivity.class);
		if (!edit) {
			intent.putExtra(EXTRA_TRIP, new Trip(NEW_TRIP_ID, "", 0, 0));
		} else {
			intent.putExtra(EXTRA_TRIP, mCurrentTrip);
		}
		startActivityForResult(intent, ADD_OR_EDIT_TRIP_REQUEST);
	}

	/**
	 * Start the activity EventActivity and pass it the current trip
	 */
	private void startEventActivity() {
		Intent intent = new Intent(this, EventActivity.class);
		intent.putExtra(EXTRA_TRIP, mCurrentTrip);
		startActivity(intent);
	}

	/********** Inner classes ***********/

	/**
	 * Map fragment that shows a map with markers for trips
	 */
	public static class CustomMapFragment extends MapFragment {
		private Map<String, Integer> markers; /* Map which maps markers ids with trips positions */

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
			View returnView = super.onCreateView(inflater, container, savedInstanceState);

			GoogleMap map = getMap();
			markers = new HashMap<String, Integer>();

			// Add markers for trips to the map
			map.clear(); // Clear all previous markers
			LatLngBounds.Builder builder;
			boolean builderEmpty;
			int placeCounter = 1;
			for (Trip t : ((TripActivity) getActivity()).mTrips) {
				builder = new LatLngBounds.Builder();
				builderEmpty = true;
				if (((TripActivity) getActivity()).mEventsPlaces.get(t.getmId()) != null) {
					// Get all events places for this trip if any
					// in order to put the marker at the center later
					for (LatLng ll : ((TripActivity) getActivity()).mEventsPlaces.get(t.getmId())) {
						double lat = ll.latitude;
						double lng = ll.longitude;
						if ((lat != 0) && (lng != 0)) {
							builder.include(new LatLng(lat, lng));
							builderEmpty = false;
						}
					}
				}
				if (!builderEmpty) {
					// Add the marker only if at least one place was selected
					// for an event of this trip
					Marker marker = map.addMarker(new MarkerOptions()
							.position(builder.build().getCenter())
							.title(t.getmName()));
					markers.put(marker.getId(), placeCounter);
				}
				placeCounter++;
			}

			// Show the corresponding trip when clicking on the marker info window
			map.setOnInfoWindowClickListener(new OnInfoWindowClickListener() {

				@Override
				public void onInfoWindowClick(Marker marker) {
					((TripActivity) getActivity()).selectItem(markers.get(marker.getId()));
				}
			});

			return returnView;
		}
	}

	/**
	 * Fragment that appears in the "content_frame", shows a trip
	 */
	public static class TripFragment extends Fragment {
		public static final String ARG_TRIP_NUMBER = "trip_number";

		private ImageAdapter mAdapter;

		public TripFragment() {
			// Empty constructor required for fragment subclasses
		}

		@Override
		public void onCreate(Bundle savedInstanceState) {
			super.onCreate(savedInstanceState);
			mAdapter = new ImageAdapter(
					getActivity(),
					((TripActivity) getActivity()).getEventsImagesFromDb(
							((TripActivity) getActivity()).mDatabaseHelper.getReadableDatabase(),
							((TripActivity) getActivity()).mTrips.get(getArguments().getInt(ARG_TRIP_NUMBER)).getmId()));
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
			final Trip trip = ((TripActivity) getActivity()).mTrips.get(getArguments().getInt(ARG_TRIP_NUMBER));

			// Update views:
			View rootView = inflater.inflate(R.layout.fragment_trip, container, false);
			DateFormat dateFormat = android.text.format.DateFormat.getMediumDateFormat(getActivity());
			
			// -> Start and end dates
			((TextView) rootView.findViewById(R.id.trip_start_date)).setText(dateFormat.format(trip.getmStartDate()));
			((TextView) rootView.findViewById(R.id.trip_end_date)).setText(dateFormat.format(trip.getmEndDate()));

			// -> Switch button
			Switch trackerSwitch = (Switch) rootView.findViewById(R.id.tracker_switch);
			trackerSwitch.setChecked(LocationTrackerService.getmTripId() != NEW_TRIP_ID);
			trackerSwitch.setEnabled(true);
			trackerSwitch.setOnCheckedChangeListener(new OnCheckedChangeListener() {
				
				@Override
				public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
					if (isChecked) {
						Intent intent = new Intent(getActivity(), LocationTrackerService.class);
						// We need to put the id of the trip we want to record
						// in the intent for the service
						intent.putExtra(TripActivity.EXTRA_TRIP_ID, trip.getmId());
						getActivity().startService(intent);
					} else {
						Intent intent = new Intent(getActivity(), LocationTrackerService.class);
						getActivity().stopService(intent);
						LocationTrackerService.setmTripId(NEW_TRIP_ID);
					}
				}
			});
			if ((LocationTrackerService.getmTripId() != NEW_TRIP_ID) && (LocationTrackerService.getmTripId() != trip.getmId())) {
				trackerSwitch.setEnabled(false);
			}

			// -> GridView
			((GridView) rootView.findViewById(R.id.trip_events_images)).setAdapter(mAdapter);

			// Set the current trip and the title
			((TripActivity) getActivity()).mCurrentTrip = trip;
			getActivity().setTitle(trip.getmName());
			return rootView;
		}
	}

	/**
	 * Dialog fragment that confirms trip deletion
	 */
	public static class DeleteDialogFragment extends DialogFragment {

		@Override
		public Dialog onCreateDialog(Bundle savedInstanceState) {
			AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
			builder.setMessage(getString(R.string.alert_lose_all_events))
					.setTitle(getString(R.string.action_discard_trip))
					.setPositiveButton(getString(R.string.yes),
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog, int id) {
									if (((TripActivity) getActivity()).deleteCurrentTrip() > 0) {
										Toast.makeText(getActivity(), getString(R.string.trip_deleted), Toast.LENGTH_SHORT).show();
									} else {
										Toast.makeText(getActivity(), getString(R.string.error), Toast.LENGTH_SHORT).show();
									}
								}
							})
					.setNegativeButton(getString(R.string.no),
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog, int id) {
									// User cancelled the dialog
									// Do nothing
								}
							});
			// Create the AlertDialog object and return it
			return builder.create();
		}
	}

	/******** Broadcast receiver ********/

	/**
	 * Broadcast receiver from LocationTrackerService
	 */
	private BroadcastReceiver onBroadcast = new BroadcastReceiver() {
	    @Override
	    public void onReceive(Context ctxt, Intent i) {
	    	((TextView) findViewById(R.id.connection_state)).setText(i.getStringExtra(LocationTrackerService.EXTRA_CONNECTION_STATE));
	    }
	};

}
