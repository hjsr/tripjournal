package fr.m2ihm.lnso.tripjournal.entities;

import java.util.HashSet;
import java.util.Set;

public class Event extends Entity {
	
	private static final long serialVersionUID = 1L;
	private long mIdTrip;
	private String mDescription;
	private long mDate;
	private EventTypeEnum mType;
	private float mMark;
	private String mPicture;
	private float mLatitude;
	private float mLongitude;
	private Set<Participant> mParticipants;
	
	/**
	 * @param mIdTrip
	 * @param mDescription
	 * @param mDate
	 * @param mType
	 * @param mMark
	 * @param mPicture
	 * @param mLatitude
	 * @param mLongitude
	 */
	public Event(long mId, String mName, long mIdTrip, String mDescription, long mDate,
			EventTypeEnum mType, float mMark, String mPicture, float mLatitude,
			float mLongitude) {
		super(mId, mName);
		this.mIdTrip = mIdTrip;
		this.mDescription = mDescription;
		this.mDate = mDate;
		this.mType = mType;
		this.mMark = mMark;
		this.mPicture = mPicture;
		this.mLatitude = mLatitude;
		this.mLongitude = mLongitude;
		this.mParticipants = new HashSet<Participant>();
	}

	public long getmIdTrip() {
		return mIdTrip;
	}

	public void setmIdTrip(long mIdTrip) {
		this.mIdTrip = mIdTrip;
	}

	public String getmDescription() {
		return mDescription;
	}

	public void setmDescription(String mDescription) {
		this.mDescription = mDescription;
	}

	public long getmDate() {
		return mDate;
	}

	public void setmDate(long mDate) {
		this.mDate = mDate;
	}

	public EventTypeEnum getmType() {
		return mType;
	}

	public void setmType(EventTypeEnum mType) {
		this.mType = mType;
	}

	public float getmMark() {
		return mMark;
	}

	public void setmMark(float mMark) {
		this.mMark = mMark;
	}

	public String getmPicture() {
		return mPicture;
	}

	public void setmPicture(String mPicture) {
		this.mPicture = mPicture;
	}

	public float getmLatitude() {
		return mLatitude;
	}

	public void setmLatitude(float mLatitude) {
		this.mLatitude = mLatitude;
	}

	public float getmLongitude() {
		return mLongitude;
	}

	public void setmLongitude(float mLongitude) {
		this.mLongitude = mLongitude;
	}

	public Set<Participant> getmParticipants() {
		return mParticipants;
	}

	public void setmParticipants(Set<Participant> mParticipants) {
		this.mParticipants = new HashSet<Participant>(mParticipants);
	}
	
}
