package fr.m2ihm.lnso.tripjournal.entities;

import java.io.Serializable;

public class Entity implements Serializable {
	
	private static final long serialVersionUID = 1L;
	private long mId;
	private String mName;
	
	public Entity(long mId, String mName) {
		super();
		this.mId = mId;
		this.mName = mName;
	}

	public long getmId() {
		return mId;
	}

	public void setmId(long mId) {
		this.mId = mId;
	}

	public String getmName() {
		return mName;
	}

	public void setmName(String mName) {
		this.mName = mName;
	}
	
}
