package fr.m2ihm.lnso.tripjournal.entities;

public class Participant extends Entity {
	
	private static final long serialVersionUID = 1L;
	private String mIdContact;
	private long mIdEvent;

	/**
	 * 
	 * @param mId
	 * @param mName
	 * @param mIdContact
	 * @param mIdEvent
	 */
	public Participant(long mId, String mName, String mIdContact, long mIdEvent) {
		super(mId, mName);
		this.mIdContact = mIdContact;
		this.mIdEvent = mIdEvent;
	}

	public String getmIdContact() {
		return mIdContact;
	}

	public void setmIdContact(String mIdContact) {
		this.mIdContact = mIdContact;
	}

	public long getmIdEvent() {
		return mIdEvent;
	}

	public void setmIdEvent(long mIdEvent) {
		this.mIdEvent = mIdEvent;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((mIdContact == null) ? 0 : mIdContact.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Participant other = (Participant) obj;
		if (mIdContact == null) {
			if (other.mIdContact != null)
				return false;
		} else if (!mIdContact.equals(other.mIdContact))
			return false;
		return true;
	}

}
