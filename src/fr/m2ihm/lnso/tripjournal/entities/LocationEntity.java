package fr.m2ihm.lnso.tripjournal.entities;

import com.google.android.gms.maps.model.LatLng;

public class LocationEntity extends Entity {

	private static final long serialVersionUID = 1L;
	private LatLng mLocation;
	private long mIdTrip;
	
	public LocationEntity(long mId, LatLng location, long tripId) {
		super(mId, null);
		this.mLocation = location;
		this.mIdTrip = tripId;
	}
	
	public LatLng getmLocation() {
		return mLocation;
	}

	public void setmLocation(LatLng mLocation) {
		this.mLocation = mLocation;
	}

	public long getmIdTrip() {
		return mIdTrip;
	}

	public void setmIdTrip(long mIdTrip) {
		this.mIdTrip = mIdTrip;
	}
	
}
