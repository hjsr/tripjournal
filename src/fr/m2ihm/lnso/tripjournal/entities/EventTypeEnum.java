package fr.m2ihm.lnso.tripjournal.entities;

public enum EventTypeEnum {
	SEE,
	DO,
	EAT,
	DRINK,
	BUY,
	SLEEP,
	OTHER
}
