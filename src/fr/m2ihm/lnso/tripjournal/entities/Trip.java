package fr.m2ihm.lnso.tripjournal.entities;

import java.util.ArrayList;
import java.util.List;

public class Trip extends Entity {
	
	private static final long serialVersionUID = 1L;
	private long mStartDate;
	private long mEndDate;
	private List<Event> mEvents;
	private List<LocationEntity> mLocations;
	
	public Trip(long mId, String mName, long mStartDate, long mEndDate) {
		super(mId, mName);
		this.mStartDate = mStartDate;
		this.mEndDate = mEndDate;
		this.mEvents = new ArrayList<Event>();
		this.mLocations = new ArrayList<LocationEntity>();
	}

	public long getmStartDate() {
		return mStartDate;
	}

	public void setmStartDate(long mStartDate) {
		this.mStartDate = mStartDate;
	}

	public long getmEndDate() {
		return mEndDate;
	}

	public void setmEndDate(long mEndDate) {
		this.mEndDate = mEndDate;
	}

	public List<Event> getmEvents() {
		return mEvents;
	}

	public void setmEvents(List<Event> mEvents) {
		this.mEvents = new ArrayList<Event>(mEvents);
	}

	public List<LocationEntity> getmLocations() {
		return mLocations;
	}

	public void setmLocations(List<LocationEntity> mLocations) {
		this.mLocations = new ArrayList<LocationEntity>(mLocations);
	}
	
}
